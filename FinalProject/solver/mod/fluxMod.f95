module fluxMod
use inputMod 
implicit none 
contains
  subroutine computeNumericalFlux(rho,v1,v2,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(4),intent(out)::numericalFlux
  real(dp),intent(in)::rho,v1,v2,p
  numericalFlux(1) = rho*v1
  numericalFlux(2) = rho*v1*v1+p
  numericalFlux(3) = rho*v1*v2
  numericalFlux(4) =  v1*(0.5_dp*rho*(v1*v1+v2*v2)+(p/(gamma-1.0_dp))+p)
  end subroutine computeNumericalFlux

  subroutine vanLeer(r,omega,deli)
  use inputMod
  implicit none
  real(dp),intent(inout) :: deli
  real(dp),intent(in) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xivl,xiR
  xivl=0.0_dp  
  if(r<0.0_dp) then
    xivl=0.0_dp
  else
    xiR=2.0_dp/(1.0_dp-omega+(1.0_dp+omega)*r)
    xivl=min((2.0_dp*r)/(1.0_dp+r),xiR)
  end if
  deli=deli*xivl
  end subroutine vanLeer
  
  subroutine superbee(r,omega,deli)
  use inputMod
  implicit none
  real(dp),intent(inout) :: deli
  real(dp),intent(in) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xi,xiR,temp
  xi = 0.0_dp
  if(r>0.0_dp) then
    xi=2.0_dp*r
  end if
  if(r>0.5_dp) then
    xi=1.0_dp
  end if
  if(r>1.0_dp) then
    temp=1.0_dp-omega+(1.0_dp+omega)*r
    xiR=2.0_dp/temp
    xi=MIN(xiR,r)
    xi=MIN(xi,2.0_dp)
  end if
  deli=deli*xi
  end subroutine superbee
end module fluxMod
