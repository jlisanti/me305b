subroutine writeSolution
use inputMod

real(dp)::rho,v1,v2,p,A
integer::n,l
character(30)::tmp

write(tmp,200)t
200 format (F3.1)
write(file_name,*)'output_t',TRIM(ADJUSTL(tmp)),'_A.dat'

open(unit=5,file=TRIM(ADJUSTL(file_name)),status='replace',action='write')
rewind(5)
do n=1,istep
  do l=1,(jmax)
    rho=U(n,l,1)
    v1=U(n,l,2)/rho
    v2=U(n,l,3)/rho
    p=(gamma-1.0_dp)*(U(n,l,4)-0.5_dp&
                      *(U(n,l,2)*(U(n,l,2)/U(n,l,1))&
                      +U(n,l,3)*(U(n,l,3)/U(n,l,1))))
    A=p/(rho**gamma)
    write(5,*)x(n,l),y(n,l),rho,v1,v2,p,A
    if(l==jmax) then
      write(5,*)
    end if
  end do
end do
close(5)

write(file_name,*)'output_t',TRIM(ADJUSTL(tmp)),'_B.dat'

open(unit=100,file=TRIM(ADJUSTL(file_name)),status='replace',action='write')
rewind(100)
do n=istep,imax
  do l=jstep,(jmax)
    rho=U(n,l,1)
    v1=U(n,l,2)/rho
    v2=U(n,l,3)/rho
    p=(gamma-1.0_dp)*(U(n,l,4)-0.5_dp&
                      *(U(n,l,2)*(U(n,l,2)/U(n,l,1))&
                      +U(n,l,3)*(U(n,l,3)/U(n,l,1))))
    A=p/(rho**gamma)
    write(100,*)x(n,l),y(n,l),rho,v1,v2,p,A
    if(l==jmax) then
       write(100,*)
    end if
  end do
end do
close(100)

end subroutine
