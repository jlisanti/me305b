subroutine MUSCL(Uij,flux,dtij)
use inputMod
use fluxMod
use errorMod
implicit none

real(dp) :: omega
real(dp),dimension(2) :: delU
real(dp),dimension(numf+4,2+dim) :: UbarL,UbarR
real(dp),dimension(2+dim) :: UL,UR,FL,FR
real(dp),dimension(numf+4,2+dim), intent(in) :: Uij
real(dp),dimension(numf+4,2+dim), intent(out) :: flux
real(dp),intent(in) :: dtij
real(dp),dimension(2+dim) :: fluxi
real(dp) :: deli
real(dp),dimension(2+dim) :: WL,WR
real(dp)::En
real(dp) :: tol
real(dp) :: r
integer::ij,l,n

do ij=numi-1,numf+1
  omega=0.0_dp
  tol=1.0e-8_dp
  do n=1,(2+dim)
    delU(1)=Uij(ij,n)-Uij(ij-1,n)
    delU(2)=Uij(ij+1,n)-Uij(ij,n)
    do l=1,2
      if(ABS(delU(l)) < tol) then
        delU(l)=tol*SIGN(1.0_dp,delU(l))
      end if
    end do
    r=delU(1)/delU(2)
    deli=0.5_dp*(1.0_dp+omega)*delU(1)&
             +0.5_dp*(1.0_dp-omega)*delU(2)
    call vanLeer(r,omega,deli)
    !call superbee(r,omega,deli)
    UL(n)=Uij(ij,n)-0.5_dp*deli
    UR(n)=Uij(ij,n)+0.5_dp*deli
  end do
  ! Left state
  WL(1)=UL(1)
  WL(2)=UL(2)/UL(1)
  WL(3)=UL(3)/UL(1)
  En=0.5_dp*(UL(2)*WL(2)+UL(3)*WL(3))
  WL(4)=(gamma-1.0_dp)*(UL(4)-En)
  ! Right state
  WR(1)=UR(1)
  WR(2)=UR(2)/UR(1)
  WR(3)=UR(3)/UR(1)
  En=0.5_dp*(UR(2)*WR(2)+UR(3)*WR(3))
  WR(4)=(gamma-1.0_dp)*(UR(4)-En)
  call computeNumericalFlux(WL(1),WL(2),WL(3),WL(4),FL)
  call computeNumericalFlux(WR(1),WR(2),WR(3),WR(4),FR)
  do n=1,(2+dim)
    UbarR(ij,n) = UL(n) + 0.5_dp*(dtij/del)*(FL(n)-FR(n))
    UbarL(ij,n) = UR(n) + 0.5_dp*(dtij/del)*(FL(n)-FR(n))
  end do
end do

do ij=numi-1,numf
  ! Solve local Riemann problem using HLLC approximate method
  ! Left state
  WL(1)=UbarL(ij,1)
  WL(2)=UbarL(ij,2)/UbarL(ij,1)
  WL(3)=UbarL(ij,3)/UbarL(ij,1)
  En=0.5_dp*(UbarL(ij,2)*WL(2)+UbarL(ij,3)*WL(3))
  WL(4)=(gamma-1.0_dp)*(UbarL(ij,4)-En)
  !  Right state
  WR(1)=UbarR(ij+1,1)
  WR(2)=UbarR(ij+1,2)/UbarR(ij+1,1)
  WR(3)=UbarR(ij+1,3)/UbarR(ij+1,1)
  En=0.5_dp*(UbarR(ij+1,2)*WR(2)+UbarR(ij+1,3)*WR(3))
  WR(4)=(gamma-1.0_dp)*(UbarR(ij+1,4)-En) 

  call computeHLLCflux(WL,WR,fluxi)

  do n=1,(2+dim)
    flux(ij,n)=fluxi(n)
    
    if(ISNAN(flux(ij,n))) then
      error=127
      call errorF(error)
      write(*,*)'fluxi ',n,'is now NAN'
      write(*,*)'flux',ij,n,'=',flux(ij,n)
      write(*,*)'inputs:'
      write(*,*)'left state',WL(1),WL(2),WL(3),WL(4)
      write(*,*)'right state',WR(1),WR(2),WR(3),WR(4)
      write(*,*)
      stop
    end if
  end do 
end do

end subroutine MUSCL
