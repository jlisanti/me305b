subroutine computeFlux(timestep,bound,ind)
use inputMod
use errorMod
implicit none

real(dp),intent(in)::timestep
real(dp)::rhotemp,utemp,vtemp,ptemp
real(dp),dimension(numf+4,dim+2)::Uij
real(dp),dimension(numf+4,dim+2)::flux
integer,dimension(2),intent(in)::bound
integer,intent(in)::ind
integer::ij

! Arrange variables for x-dir 1D Sweep
if(dir==2) then
  do ij=numi-2,numf+2
    rhotemp=U(ij,ind,1)
    utemp=U(ij,ind,2)/rhotemp
    vtemp=U(ij,ind,3)/rhotemp
    ptemp=(gamma-1.0_dp)*(U(ij,ind,4)-0.5_dp&
         *(U(ij,ind,2)*(U(ij,ind,2)/U(ij,ind,1))&
          +U(ij,ind,3)*(U(ij,ind,3)/U(ij,ind,1))))
    Uij(ij,1)=rhotemp
    Uij(ij,2)=rhotemp*utemp
    Uij(ij,3)=rhotemp*vtemp
    Uij(ij,4)=rhotemp*0.5_dp*(utemp*utemp+vtemp*vtemp)&
            +ptemp/(gamma-1.0_dp)
    if(rhotemp>prim(1)) prim(1)=rhotemp
    if(rhotemp<prim(2)) prim(2)=rhotemp
    if(ptemp>prim(7)) prim(7)=ptemp
    if(ptemp<prim(8)) prim(8)=ptemp
  end do
else
! Arrange variables for y-dir 1D Sweep
  do ij=numi-2,numf+2
    rhotemp=U(ind,ij,1)
    utemp=U(ind,ij,3)/rhotemp
    vtemp=U(ind,ij,2)/rhotemp
    ptemp=(gamma-1.0_dp)*(U(ind,ij,4)-0.5_dp&
         *(U(ind,ij,2)*(U(ind,ij,2)/U(ind,ij,1))&
          +U(ind,ij,3)*(U(ind,ij,3)/U(ind,ij,1))))
    Uij(ij,1)=rhotemp
    Uij(ij,2)=rhotemp*utemp
    Uij(ij,3)=rhotemp*vtemp
    Uij(ij,4)=rhotemp*0.5_dp*(utemp*utemp+vtemp*vtemp)&
            +ptemp/(gamma-1.0_dp)
    if(rhotemp>prim(1)) prim(1)=rhotemp
    if(rhotemp<prim(2)) prim(2)=rhotemp
    if(ptemp>prim(7)) prim(7)=ptemp
    if(ptemp<prim(8)) prim(8)=ptemp
  end do
end if

call imposeBoundaries(Uij,bound)
call MUSCL(Uij,flux,timestep)
call updateCells(Uij,flux,timestep)

! Store 1D x-direction solution
if(dir==2) then
  do ij=numi-2,numf+2
    rhotemp=Uij(ij,1)
    utemp=Uij(ij,2)/rhotemp
    vtemp=Uij(ij,3)/rhotemp
    ptemp=(gamma-1.0_dp)*(Uij(ij,4)-0.5_dp&
         *(Uij(ij,2)*(Uij(ij,2)/Uij(ij,1))&
          +Uij(ij,3)*(Uij(ij,3)/Uij(ij,1))))
   
   U(ij,ind,1)=rhotemp
   U(ij,ind,2)=rhotemp*utemp
   U(ij,ind,3)=rhotemp*vtemp
   U(ij,ind,4)=rhotemp*0.5_dp*(utemp*utemp+vtemp*vtemp)&
            +ptemp/(gamma-1.0_dp)
  end do
else
! Store 1D y-direction solution
  do ij=numi-2,numf+2
    rhotemp=Uij(ij,1)
    utemp=Uij(ij,3)/rhotemp
    vtemp=Uij(ij,2)/rhotemp
    ptemp=(gamma-1.0_dp)*(Uij(ij,4)-0.5_dp&
         *(Uij(ij,2)*(Uij(ij,2)/Uij(ij,1))&
          +Uij(ij,3)*(Uij(ij,3)/Uij(ij,1))))
   U(ind,ij,1)=rhotemp
   U(ind,ij,2)=rhotemp*utemp
   U(ind,ij,3)=rhotemp*vtemp
   U(ind,ij,4)=rhotemp*0.5_dp*(utemp*utemp+vtemp*vtemp)&
            +ptemp/(gamma-1.0_dp)
  end do
end if

end subroutine computeFlux
