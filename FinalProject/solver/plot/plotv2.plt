#!/bin/gnuplot -persist
set term post enhanced eps "Times,18" solid
set term postscript enhanced color
set pointsize 1.5
set cntrparam levels 30
set contour base
unset sur
set view map
set dgrid3d
set xrange [0:3]
set yrange [0:1]
set out "density.eps"
set iso 100
set samp 100
set key rmargin
splot "output.dat" notitle
