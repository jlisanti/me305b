%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short Sectioned Assignment
% LaTeX Template
% Version 1.0 (5/5/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[paper=a4, fontsize=11pt]{scrartcl} % A4 paper and 11pt font size

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage{fourier} % Use the Adobe Utopia font for the document - comment this line to return to the LaTeX default
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages

\usepackage{graphicx}
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{caption}

\usepackage{sectsty} % Allows customizing section commands
%\allsectionsfont{\centering \normalfont\scshape} % Make all sections centered, the default font and small caps
\allsectionsfont{\normalfont\scshape} % Make all sections centered, the default font and small caps

\usepackage{fancyhdr} % Custom headers and footers
\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
\fancyhead{} % No page header - if you want one, create it in the same way as the footers below
\fancyfoot[L]{} % Empty left footer
\fancyfoot[C]{} % Empty center footer
\fancyfoot[R]{\thepage} % Page numbering for right footer
\renewcommand{\headrulewidth}{0pt} % Remove header underlines
\renewcommand{\footrulewidth}{0pt} % Remove footer underlines
\setlength{\headheight}{13.6pt} % Customize the height of the header

\numberwithin{equation}{section} % Number equations within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{figure}{section} % Number figures within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{0pt} % Removes all indentation from paragraphs - comment this line for an assignment with lots of text

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{King Abdullah University of Science and Technology} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\LARGE ME305b Final Project \\ % The assignment title
\horrule{1pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Joel Lisanti} % Your name

\date{\normalsize\today} % Today's date or a custom date

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	PROBLEM 1
%----------------------------------------------------------------------------------------

\section{Introduction}
\subsection{Governing Equations}
The objective of this work was to implement a numerical solution to the two-dimensional, time-dependent Euler equations, with an ideal equation of state, that has effectively captures discontinuities in a flow field with strong shocks.
\\

The conservative formulation of the two dimensional Euler equations, in differential form, are given as in equation 1.1.  
\begin{equation}
U_{t}+F(U)_{x}+G(U)_{y}=0
\end{equation}
where U is the vector of conserved variables and F(U) and G(U) are the vectors of conserved fluxes in the x and y direction respectively.  
\begin{equation}
U=\left[\begin{array}{c} \rho \\ \rho u \\ \rho v \\ E \end{array} \right]
\end{equation}
\begin{equation}
F=\left[\begin{array}{c} \rho u \\ \rho u^{2} + p \\ \rho uv \\ u(E + p)\end{array} \right]
\end{equation}
\begin{equation}
G=\left[\begin{array}{c} \rho v \\ \rho uv \\ \rho v^{2} + p \\ v(E + p) \end{array} \right]
\end{equation}
Notation is given as $\rho$ for density, $p$ for pressure, $u$ for x-direction velocity, $v$ for y-direction velocity and E for total energy per unit volume. 

\subsection{Numerical Method}
The numerical approach used in this work is based upon Godunov's methods.  This approach allows for the effective capture of discontinuities in the flow by introducing explicit nonlinearity into the difference method.  Second accuracy was achieved through implementation of the MUSCL-Hancock TVD scheme with van-Leer's slope limiter and the second order dimensional splitting scheme, equation 1.5.  The solution was found to be independent of the choice of slope limiter. Uniform grid spacing was used in both the x and y directions.

\begin{equation}
U^{n+1}=\chi^{\frac{1}{2}\Delta t}\gamma^{\Delta t}\chi^{\frac{1}{2}\Delta t}
\end{equation}
\\

The HLLC approximate Riemann solver was chosen for it's relative speed and ability to capture weak contact discontinuities.  For all cases the time step was computed using approximate wave speeds, for this reason CFL ramping (from 0.2 to 0.8) was implemented to allow for a stable solution startup.  Solutions were found to be independent of CFL within the stability limit. 
\\

Specific details regarding the method implemented in this work and the HLLC approximate Riemann solver are available in reference [5].
\\

The following is a high level overview of the implementation of the described numerical method. 

\begin{enumerate}
	\item Generate Mesh
	\item Initialize Domain
	\item Advance Solution in Time
		\begin{enumerate}
			\item Preform 1D x-sweep at half time step
			\begin{enumerate}
				\item Set boundary conditions 
				\item Compute flux via MUSCL-Hancock TVD scheme
				\item Update solution
			\end{enumerate}
			\item Preform 1D y-sweep at full time step 
			\begin{enumerate}
				\item Repeat as before
			\end{enumerate}
			\item Preform 1D x-sweep at half time step
			\begin{enumerate}
				\item Repeat as before
			\end{enumerate}
		\end{enumerate}
	\item Write solution
\end{enumerate}

\section{Problem Background}
\subsection{Overview}
This two-dimensional test problem, for which an overview is displayed in figure 2.1, is useful in it's relative (though non physical) set up. The domain is defined as a tunnel of 1 unit height, 3 unit lengths long, and infinite width in the direction orthogonal to the plane.  The step is 0.2 units high and 0.6 units from the left (inflow) boundary.
\\

\begin{figure}[h]
  \centering
    \includegraphics[width=0.625\textwidth]{img/Overview.png}
    \caption{Test Case Overview}
\end{figure}

The flow field is initialized everywhere with an ideal gas, with $\gamma = 1.4$, density of 1.4, pressure of 1.0, and velocity of 3 (mach 3).  Gas with these properties is continually fed  from the left boundary.  The right boundary is set with transmissive boundary conditions and the walls are set with reflecting boundary conditions. 
\\
\subsection{Previous Studies}

This test problem was first presented by Emery [1] in 1968 for comparison of serval schemes relevant at the time. Although these schemes were not able to resolve the sharp features present in the complex flow field, they provided a sound qualitative physical solution.  The problem has maintained popularity as it is reused for comparing methods. 
\\[10.5cm]

\begin{figure}[h]
  \centering
    \includegraphics[width=0.925\textwidth]{img/Emery_pressure.png}
    \caption{Pressure contours produced by Emery (1968)}
\end{figure}

Further work was present by van Leer [2] with a solution to the original MUSCL code for this problem.  Results obtained with a single-step Eulerian MUSCL are present by Colella [3] and a review of the various schemes and their respect results are present by Woodward and Collela [4].
\\

\begin{figure}[h]
  \centering
    \includegraphics[width=0.925\textwidth]{img/Leer_pressure.png}
    \caption{Pressure contours produced by van-Leer (1978)}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.925\textwidth]{img/Colella_density.png}
    \caption{Density contours produced by Woodward and Colella (1983)}
\end{figure}

\section{Results and Discussion}
\subsection{Previous Studies}
The computed solution compares very well with previous results and matches physical intuition.  Density and pressure contours are presented for display of the flow structure and comparison with previous studies (both quantitative and qualitative). Grid spacing of dy=dx=1/20, 1/40, 1/80, and 1/500 were evaluated to establish grid independence.  All figures are shown for the 1/500 grid case.

Figure (3.1) details the progression of the flow field structure with time.  The density contours are plotted at four time intervals, 0.5, 1.0, 1.5, and 2.0.  It is clear in these figures that a bow shock is  formed by time = 0.5 units and, as the flow progresses, a train of reflected shocks is formed through the channel.

\begin{figure}[h!]
 \centering
\includegraphics[width=1.\textwidth]{img/Rho_time.png}
\caption{Time progression of flow field}
\end{figure}

Figures (3.2) and (3.3) display the density and pressure contours.  These results are shown to compare very well with previous studies.  The flow structure is shown to be a bow shock directly in front of the step with a mach stem connecting a train of reflected shocks.
  
\begin{figure}[h!]
 \centering
\includegraphics[width=1.\textwidth]{img/P.png}
\caption{Pressure contours at t = 4.0 units}
\end{figure}

\begin{figure}[h!]
 \centering
\includegraphics[width=1.\textwidth]{img/Rho.png}
\caption{Density contours at t = 4.0 units}
\end{figure}

Figure (3.4) provides a plot of an entropy constant and highlights the numerical error induced by the singular point at the corner of the step.  This numerical error results in a boundary layer of a single cell height that propagates downstream and interacts with the shock train. 

\begin{figure}[h!]
 \centering
\includegraphics[width=1.\textwidth]{img/A.png}
\caption{Contours of $A=p/{\rho}^{\gamma}$ at t = 4.0 units}
\end{figure}


\section{References}

[1] A. E. Emery, J. Comput. Phys. 2 (1968), 306. 
\\

[2] B. val Leer, J. Comput. Phys. 32 (1979), 101. 
\\

[3] P. Colella, A Direct-Eulerian MUSCL scheme for gas dynamics, SIAM J. Sci. Statist. Comput.
\\

[4] P. Woodward, P. Colella, J. Comput. Phys. 54 (1984), 115.
\\

[5] E. F. Toro, Riemann solvers and numerical methods for fluid dynamics: a practical introduction, 1994. Springer.
\end{document}