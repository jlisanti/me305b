program twoDEuler
use inputMod
implicit none

! Generate mesh
call mesh

! Initialize domain
call initializeDomain

! Fill arrays
prim(1)=0.0_dp
prim(2)=10.0_dp
prim(3)=0.0_dp
prim(4)=10.0_dp
prim(5)=0.0_dp
prim(6)=10.0_dp
prim(7)=0.0_dp
prim(8)=10.0_dp


! Advance solution in time
do k=1,maxiter

  ! Impose CFL
  call imposeCFL 

  ! Step time forward
  t = t + dt
  twrite=twrite+dt

  ! Advance Solution
  call advanceSolution
  ! Check if output time is reached

  if(t>=tout) then 
    ! Write solution to file 
    write(*,*)'Time= ',t
    write(*,*)'Output time reached, exiting...'
    call writeSolution 
    exit
  end if

  ! Print solution progress
  if(counter==50) then 
    write(*,*)
    write(*,100)'Iteration',k,'t=',t,' dt=',dt
    write(*,110)'Max rho ',prim(1),'Min rho',prim(2)
    write(*,110)'Max p   ',prim(7),'Min p  ',prim(8)
    counter=0
    100 format (A10,I5,A3,F5.3,A4,ES10.3)
    110 format (A12,F5.2,A12,F5.2)
    write(*,*)
  end if
  counter=counter+1 

  ! Write solution at 0.5 t intervals
  if(twrite>0.5) then
    call writeSolution
    twrite=0.0_dp
  end if
end do

end program twoDEuler
