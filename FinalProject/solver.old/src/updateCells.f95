subroutine updateCells(Uij,flux,dtij)
use inputMod
use errorMod
implicit none

integer::n,l
REAL(dp),DIMENSION(numf+4,dim+2),INTENT(inout)::Uij
REAL(dp),DIMENSION(numf+4,dim+2),INTENT(in)::flux
REAL(dp),INTENT(in)::dtij

do n=numi,numf
  do l=1,(dim+2) 
    Uij(n,l)=Uij(n,l)+(dtij/del)*(flux(n-1,l)-flux(n,l))
    if(ISNAN(Uij(n,l))) then
      error=126
      call errorF(error)
      write(*,*)'U ',l,'is now NAN'
      write(*,*)'flux i',l,'=',flux(n,l)
      write(*,*)'flux i-1',l,'=',flux(n-1,l)
      write(*,*)
      stop
    end if
  end do 
end do

end subroutine updateCells
