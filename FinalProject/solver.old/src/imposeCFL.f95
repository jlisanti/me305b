subroutine imposeCFL
use inputMod
implicit none
real(dp)::SxMax,SyMax
real(dp)::CFL
real(dp)::dts

call computeSmax(SxMax,SyMax)

if(k<1000) then
  CFL=0.1_dp
else
  CFL=0.8_dp
end if

dts=MIN(del/SxMax,del/SyMax)

dt=CFL*dts

if((dt+t)>tout) then
  dt=tout-t
end if

end subroutine imposeCFL
