subroutine mesh
use inputMod
implicit none
real(dp)::xi,yj
real(dp)::xstep,ystep
real(dp)::Length,Height
integer::xcells,ycells
integer::n

! Read in mesh file
open(unit=1,file='meshDict',status='old',action='read')
rewind(1)
do n=1,37
  if(n==7) then
    read(1,*) ystep
  elseif(n==10) then 
    read(1,*) xstep
  elseif(n==13) then 
    read(1,*) Length
  elseif(n==16) then 
    read(1,*) Height
  !elseif(n==19) then
  !  read(1,*) xcells
  !elseif(n==22) then 
  !  read(1,*) ycells
  elseif(n==25) then
    read(1,*) tout
  elseif(n==28) then
    read(1,*) rhoi
  elseif(n==31) then
    read(1,*) veli
  elseif(n==34) then
    read(1,*) pi
  elseif(n==37) then
    read(1,*) del
  else 
    read(1,*)
  end if
end do
close(1)
! Allocate arrays
!allocate(x(xcells+4,ycells+4))
!allocate(y(xcells+4,ycells+4))
!allocate(U(xcells+4,ycells+4,4))
! Compute necessary quantities
!imax=xcells+4
!jmax=ycells+4
!dx=real(Length/real(imax))
!dy=real(Height/real(jmax))
xcells=int(Length/del)+1
ycells=int(Height/del)+1
allocate(x(xcells,ycells))
allocate(y(xcells,ycells))
allocate(U(xcells,ycells,4))
imax=xcells
jmax=ycells
istep=int(xstep/del)+1
jstep=int(ystep/del)+1
write(*,*)del
write(*,*)'xstep',xstep,'ystep',ystep
write(*,*)'istep',istep,'jstep',jstep
! Fill domain prior to step
do i=1,imax
  xi=(real(i)-1.0_dp)*del
  do j=1,jmax
    yj=(real(j)-1.0_dp)*del
    x(i,j)=xi
    y(i,j)=yj 
  end do
end do

! Fill domain aft of step
!do i=istep,imax
!  xi=(real(i)-1.0_dp)*del
!  do j=jstep,jmax
!    yj=(real(j)-1.0_dp)*del
!    x(i,j)=xi
!    y(i,j)=yj
!  end do
!end do

write(*,*)'Mesh info'
write(*,*)'jstep',jstep
write(*,*)'jmax',jmax
write(*,*)'istep',istep
write(*,*)'imax',imax
write(*,*)'delta',del

end subroutine
