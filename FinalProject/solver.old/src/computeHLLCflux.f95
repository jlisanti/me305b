!------------------------------------------------------------------!
!  The purpose of this subroutine is to compute numerical
!    flux using the HLLC approximate solution to the
!    Riemann problem.
!------------------------------------------------------------------!
subroutine computeHLLCflux(WL,WR,flux)
use inputMod
use errorMod
use HLLCMod
implicit none
real(dp),dimension(2+dim),intent(in)::WL,WR
real(dp),dimension(2+dim),intent(inout)::flux
real(dp),dimension(2+dim)::numericalFlux
real(dp),dimension(2+dim)::Ustar,Ul,Ur
real(dp)::SL,Sstar,SR
integer::n,ij
rhoL=WL(1)
uvelL=WL(2)
vvelL=WL(3)
pL=WL(4)

rhoR=WR(1)
uvelR=WR(2)
vvelR=WR(3)
pR=WR(4)

call soundSpeed(rhoL,pL,gamma,aL)
call soundSpeed(rhoR,pR,gamma,aR)
call estimateWaveSpeeds(SL,Sstar,SR)

if(SL>0.0_dp) then
  call computeNumericalFlux(rhoL,uvelL,vvelL,pL,numericalFlux)
  do n=1,(2+dim)
    flux(n)=numericalFlux(n)
  end do
end if
  
if((SL<0.0_dp) .and. (SR>0.0_dp)) then
  if(Sstar>0.0_dp) then
    call computeNumericalFlux(rhoL,uvelL,vvelL,pL,numericalFlux)
    call computeU(rhoL,uvelL,vvelL,pL,Ul)
    call computeUstar(Sstar,SL,rhoL,uvelL,vvelL,pL,Ustar)
    do n=1,(2+dim)
      flux(n)=numericalFlux(n)+SL*(Ustar(n)-Ul(n))
    end do
  else
    call computeNumericalFlux(rhoR,uvelR,vvelR,pR,numericalFlux)
    call computeU(rhoR,uvelR,vvelR,pR,Ur)
    call computeUstar(Sstar,SR,rhoR,uvelR,vvelR,pR,Ustar)
    do n=1,(2+dim)
      flux(n)=numericalFlux(n)+SR*(Ustar(n)-Ur(n))
    end do
  end if
end if
  
if(SR<0.0_dp) then
 call computeNumericalFlux(rhoR,uvelR,vvelR,pR,numericalFlux)
  do n=1,(2+dim)
    flux(n)=numericalFlux(n)
  end do
end if

 do n=1,(2+dim)
  if(ISNAN(flux(n))) then
    error=128
    call errorF(error)
    write(*,*)'fluxi ',n,'is now NAN'
    write(*,*)'flux',ij,n,'=',flux(n)
    write(*,*)'inputs:'
    write(*,*)'left state',rhoL,uvelL,pL
    write(*,*)'right state',rhoR,uvelR,pR
    write(*,*)'numericalFlux',numericalFlux(n)
    write(*,*)'SR',SR
    write(*,*)'Ustar',Ustar(n)
    write(*,*)'Ur',Ur(n)
    write(*,*)
    stop
  end if
end do

end subroutine computeHLLCflux
