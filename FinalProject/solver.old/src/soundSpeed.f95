SUBROUTINE soundSpeed(rho,p,gamma,a)
IMPLICIT NONE
integer, parameter :: dp = selected_real_kind(15, 307)
real(dp),intent(in)::rho,p,gamma
real(dp),intent(out)::a
a=SQRT(gamma*p/rho)
END SUBROUTINE soundSpeed
