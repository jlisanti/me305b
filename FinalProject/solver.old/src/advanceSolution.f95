subroutine advanceSolution
use inputMod
implicit none

real(dp)::dtsplit
integer,dimension(2)::bound

bound(1)=0
bound(2)=0

!write(*,*)'first x sweep'
! Half time step x
dtsplit=0.5_dp*dt
do j=1,jmax
  ! define x-sweep domain
  ! set boundary conditions for x-sweep
  if(j<jstep) then
    bound(1)=1
    bound(2)=3
    numi = 3
    numf = istep-2
  else
    bound(1)=1
    bound(2)=2
    numi = 3
    numf = imax-2
  end if
  dir=2
  
  ! solve 1-D problem in x-direction
  call computeFlux(dtsplit,bound,j)
end do

! Full time step y
dtsplit=dt
do i=1,imax
  ! define y-sweep domain
  ! set boundary conditions for y-sweep
  if(i<(istep)) then
    bound(1)=3
    bound(2)=3
    numi = 3
    numf = jmax-2
  else
    bound(1)=3
    bound(2)=3
    numi = jstep+2
    numf = jmax-2
  end if
  dir=1
  ! solve 1-D problem in y-direction 
  call computeFlux(dtsplit,bound,i)
end do

! Half time step x
dtsplit=0.5_dp*dt
do j=1,jmax
  ! define x-sweep domain
  ! set boundary conditions for x-sweep
  if(j<jstep) then
    bound(1)=1
    bound(2)=3
    numi = 3
    numf = istep-2
  else
    bound(1)=1
    bound(2)=2
    numi = 3
    numf = imax-2
  end if
  dir=2
  ! solve 1-D problem in x-direction
  call computeFlux(dtsplit,bound,j)
end do

end subroutine advanceSolution
