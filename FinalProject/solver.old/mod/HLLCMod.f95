module HLLCMod
use inputMod 
implicit none 

real(dp)::rhoL,uvelL,vvelL,pL
real(dp)::rhoR,uvelR,vvelR,pR
real(dp)::aL,aR

contains
  subroutine computeNumericalFlux(rho,v1,v2,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(4),intent(out)::numericalFlux
  real(dp),intent(in)::rho,v1,v2,p
  real(dp)::H,specificEnergy,U4
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U4=rho*(0.5_dp*(v1*v1+v2*v2)+specificEnergy)
  H=U4+p
  numericalFlux(1) = rho*v1
  numericalFlux(2) = (rho*v1)*v1+p
  numericalFlux(3) = rho*v1*v2
  numericalFlux(4) = H*v1
  end subroutine computeNumericalFlux

  subroutine computeU(rho,v1,v2,p,Utemp)
  use inputMod
  implicit none
  real(dp),intent(in)::rho,v1,v2,p
  real(dp),dimension(4),intent(out)::Utemp
  !real(dp)::specificEnergy
  Utemp(1)=rho
  Utemp(2)=rho*v1
  Utemp(3)=rho*v2
  Utemp(4)=rho*0.5_dp*(v1*v1+v2*v2)+(p/(gamma-1.0_dp))
  end subroutine computeU

  subroutine computeUStar(Sstar,Sk,rho,v1,v2,p,Ustar)
  use inputMod
  implicit none
  real(dp),intent(in)::Sstar,Sk,rho,v1,v2,p
  real(dp),dimension(4),intent(out)::Ustar
  real(dp)::A,B,C
  real(dp)::se
  se=p/((gamma-1.0_dp)*rho)
  A=rho*((Sk-v1)/(Sk-Sstar))
  B=(0.5_dp*((v1*v1+v2*v2))+se)
  C=Sstar+(p/(rho*(Sk-v1)))
  Ustar(1)=A
  Ustar(2)=A*Sstar
  Ustar(3)=A*v2
  Ustar(4)=A*(B+(Sstar-v1)*C)
  end subroutine computeUStar

  subroutine estimateWaveSpeeds(SL,Sstar,SR)
  use inputMod
  use errorMod
  implicit none

  ! Data dictionary
  real(dp),intent(out)::SL,Sstar,SR
  REAL(dp) :: c1,c2,c3,c4,c5,c6,c7,c8
  REAL(dp) :: gL,gR
  REAL(dp) :: pMin,pMax,qMax
  REAL(dp) :: ppv
  real(dp) ::cup
  real(dp) ::pq,PTL,PTR
  real(dp) :: po
  real(dp) ::um
  integer::ca
  c1 = (gamma-1.0_dp)/(2.0_dp*gamma)
  c2 = (gamma+1.0_dp)/(2.0_dp*gamma)
  c3 = 2.0_dp*gamma/(gamma-1.0)
  c4 = 2.0_dp/(gamma-1.0_dp)
  c5 = 2.0_dp/(gamma+1.0_dp)
  c6 = (gamma-1.0_dp)/(gamma+1.0_dp)
  c7 = (gamma-1.0_dp)/2.0_dp
  c8 = gamma-1.0_dp

  ! Compute necessary quantities
  pMin = MIN(pL,pR)
  pMax = MAX(pL,pR)
  qMax = pMax/pMin
  cup=0.25_dp*(rhoL+rhoR)*(aL+aR)
  ppv=MAX(0.5_dp*(pL+pR)+0.5_dp*(uvelL-uvelR)*cup,0.0_dp)
  
  if(ISNAN(qMax)) then
    error=102
    call errorF(error)
    write(*,*)'i',i,'j',j,'pmax',pMax,'pMin',pMin
    write(*,*)'t',t
    write(*,*)'numi',numi,'numf',numf
    !write(*,*)U(i,j,1),U(i,j,2),U(i,j,3),U(i,j,4)
    stop
  end if

  IF((qMax<2.0_dp).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
    po = ppv
    um=0.5_dp*(uvelL+uvelR)+0.5_dp*(pL-pR)/cup
    ca=1
  ELSEIF(ppv<pMin) THEN
    ! Two-Rarefaction 
    pq  = (pL/pR)**c1
    um  = (pq*uvelL/aL + uvelR/aR + c4*(pq - 1.0_dp))/(pq/aL + 1.0_dp/aR)
    PTL = 1.0_dp + c7*(uvelL - um)/aL
    PTR = 1.0_dp + c7*(um - uvelR)/aR
    po  = 0.5_dp*(pL*PTL**c3 + pR*PTR**c3)
    ca=2
  ELSE
    ! Two-Shock
    gL=sqrt((c5/rhoL)/(c6*pL+ppv))
    gR=SQRT((c5/rhoR)/(ppv+c6*pR))
    po=(gL*pL+gR*pR-(uvelR-uvelL))/(gL+gR)
    um=0.5_dp*(uvelL+uvelR)+0.5_dp*(gR*(po-pR)-gL*(po-pL))
    ca=3
  END IF
  if(po<pL) then
    SL = uvelL-aL
  else
    SL = uvelL-aL*SQRT(1.0_dp+c2*(po/pL-1.0_dp))
  endif
  Sstar=um
  if(po<pR) then
    SR = uvelR+aR
  else
    SR = uvelR+aR*SQRT(1.0_dp+c2*(po/pR-1.0_dp))
  endif
  end subroutine estimateWaveSpeeds
end module HLLCMod
