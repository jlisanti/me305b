#!/bin/gnuplot -persist
 #set term post enhanced eps "Times,18" solid
 set term postscript enhanced color
 #set pointsize 1.5
 set output "plot.ps"
 #set key bottom right
 #set xlabel "x"
 #set ylabel "z"
 #set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
 #set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"
 #set out "Temps.eps"
 #set pm3d map
 #set cbrange [2:6.5]
 #set cntrparam level incremental 0, 0.01, 7.5
 #set palette rgbformulae 22,13,10
 #splot "output.dat" notitle
 # Set various features of the plot
set pm3d
#set dgrid3d 40,60
unset surface  # don't need surfaces
set view map
set contour
set key outside
set cntrparam cubicspline  # smooth out the lines
set cntrparam levels 50    # sets the num of contour lines
set pm3d interpolate 20,20
set palette model RGB defined ( 0"black", 1"blue", 2"cyan",3"green",4"yellow",\
     5"red",8"purple" )
set format x "%.1f"
set format y "%.1f"
set format z "%.2f"
 splot "output.dat" using 1:2:3 notitle with lines lt 1
