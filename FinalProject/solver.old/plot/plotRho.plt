# Set terminal and output
set terminal postscript enhanced color
set output "plot.eps"
 
# Set various features of the plot
set pm3d
unset surface  # don't need surfaces
set view map
set contour
set key outside
set cntrparam cubicspline  # smooth out the lines
#set cntrparam levels 30    # sets the num of contour lines
set cntrparam levels incremental  .5,0.25,7.5
#set pm3d interpolate 20,20 # interpolate the color
set cbrange [.9:7.5]
set size ratio 0.33
#show isosamples
# Set a nice color palette
#set palette model RGB defined ( 0"black", 1"blue", 2"cyan",3"green",4"yellow",\
#     5"red",8"purple" )
 
# Axes
#set xlabel "COC angle (degree)"
#set ylabel "C-H distance (angstrom)
#set format x "%.1f"
#set format y "%.1f"
#set format z "%.2f"
set style line 1 lc rgb "black" lw 1
set style line 2 lc rgb "black" lw 2 
set style line 3 lc rgb "black" lw 2 
set style line 4 lc rgb "black" lw 2 
set style line 5 lc rgb "black" lw 2 
set style line 6 lc rgb "black" lw 2  
# Now plot
splot "output_t20.dat" using 1:2:3 notitle with lines
