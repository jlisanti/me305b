#Set terminal and output
set terminal postscript enhanced color
set output "A.eps"

set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_A.dat'
splot "output_t4.0_A.dat" using 1:2:7
unset table

set contour base
set cntrparam level incremental .2,.25,10
unset surface
set table 'tmp_cont_data_A.dat'
splot "output_t4.0_A.dat" using 1:2:7
unset table

reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_B.dat'
splot "output_t4.0_B.dat" using 1:2:7
unset table

set contour base
set cntrparam level incremental .2,0.25,10
unset surface
set table 'tmp_cont_data_B.dat'
splot "output_t4.0_B.dat" using 1:2:7
unset table

reset 
set size ratio -1
set cbrange [0.2:7.5]
set xrange [0:3]
set yrange [0:1]
set title "t = 4.0"
set xlabel "x"
set ylabel "y"
unset key
p 'tmp_data_A.dat' with image, 'tmp_cont_data_A.dat' w l lt -1 lw .1,\
  'tmp_data_B.dat' with image, 'tmp_cont_data_B.dat' w l lt -1 lw .1
