#Set terminal and output
set term post enhanced color eps "Times,8" solid
set output "Rho_time_evolution.eps"

# Time = 1
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_A_1.dat'
splot "output_t0.5_A.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_A_1.dat'
splot "output_t0.5_A.dat"
unset table

reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_B_1.dat'
splot "output_t0.5_B.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_B_1.dat'
splot "output_t0.5_B.dat"
unset table

#Time = 2
reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_A_2.dat'
splot "output_t1.0_A.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_A_2.dat'
splot "output_t1.0_A.dat"
unset table

reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_B_2.dat'
splot "output_t1.0_B.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_B_2.dat'
splot "output_t1.0_B.dat"
unset table

#Time = 3
reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_A_3.dat'
splot "output_t1.5_A.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_A_3.dat'
splot "output_t1.5_A.dat"
unset table

reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_B_3.dat'
splot "output_t1.5_B.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_B_3.dat'
splot "output_t1.5_B.dat"
unset table

#Time = 4
reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_A_4.dat'
splot "output_t2.0_A.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_A_4.dat'
splot "output_t2.0_A.dat"
unset table

reset
set xrange [0:3]
set yrange [0:1]
set table 'tmp_data_B_4.dat'
splot "output_t2.0_B.dat"
unset table

set contour base
set cntrparam level incremental .2,0.5,10
unset surface
set table 'tmp_cont_data_B_4.dat'
splot "output_t2.0_B.dat"
unset table

reset 
set size ratio -1
set cbrange [.2:7]
set xrange [0:3]
set yrange [0:1]
unset key

set multiplot layout 4,1

set rmargin 0.01
set lmargin 0.01

set title "density contours t = 0.5"
p 'tmp_data_A_1.dat' with image, 'tmp_cont_data_A_1.dat' w l lt -1 lw .1,\
  'tmp_data_B_1.dat' with image, 'tmp_cont_data_B_1.dat' w l lt -1 lw .1

set title "density contours t = 1.0"
p 'tmp_data_A_2.dat' with image, 'tmp_cont_data_A_2.dat' w l lt -1 lw .1,\
  'tmp_data_B_2.dat' with image, 'tmp_cont_data_B_2.dat' w l lt -1 lw .1

set title "density contours t = 1.5"
p 'tmp_data_A_3.dat' with image, 'tmp_cont_data_A_3.dat' w l lt -1 lw .1,\
  'tmp_data_B_3.dat' with image, 'tmp_cont_data_B_3.dat' w l lt -1 lw .1

set title "density contours t = 2.0"
p 'tmp_data_A_4.dat' with image, 'tmp_cont_data_A_4.dat' w l lt -1 lw .1,\
  'tmp_data_B_4.dat' with image, 'tmp_cont_data_B_4.dat' w l lt -1 lw .1

unset multiplot
