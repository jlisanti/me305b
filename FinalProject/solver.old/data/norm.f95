program computeNorm
implicit none
integer, parameter :: dp = selected_real_kind(15, 307)
real(dp)::a,b,c,d,e,f,g,h
real(dp)::norma,normb,normc,normd
real::d1,d2
integer::imax=100000000
integer::status
integer::i
character(5)::file_name_a,file_name_b,file_name_c,file_name_d

file_name_a="output_t4.0_A.dat"
file_name_b="output_t4.0_B.dat"

write(*,*)'Enter "A" filename'
read(*,*)file_name_c

write(*,*)'Enter "B" filename'
read(*,*)file_name_D

!file_name_c=output_t4.0_C.dat
!file_name_d=output_t4.0_D.dat

open(unit=1,file=TRIM(ADJUSTL(file_name_a)),status='old',action='read',iostat=status)
open(unit=2,file=TRIM(ADJUSTL(file_name_c)),status='old',action='read')
rewind(1)
rewind(2)
do i=1,imax
  read(1,*,IOSTAT=status)d1,d2,a,b,c,d
  read(2,*)d1,d2,e,f,g,h
  norma=norma+(abs(a)-abs(e))**2.
  normb=normb+(abs(b)-abs(f))**2.
  normc=normc+(abs(c)-abs(g))**2.
  normd=normd+(abs(d)-abs(h))**2.
  if(status /= 0) exit
end do
close(1)
close(2)

open(unit=3,file=TRIM(ADJUSTL(file_name_a)),status='old',action='read',iostat=status)
open(unit=4,file=TRIM(ADJUSTL(file_name_d)),status='old',action='read')
rewind(3)
rewind(4)
do i=1,imax
  read(3,*,IOSTAT=status)d1,d2,a,b,c,d
  read(4,*)d1,d2,e,f,g,h
  norma=norma+(abs(a)-abs(e))**2.
  normb=normb+(abs(b)-abs(f))**2.
  normc=normc+(abs(c)-abs(g))**2.
  normd=normd+(abs(d)-abs(h))**2. 
  if(status /= 0) exit
end do
close(3)
close(4)
write(*,*)norma,normb,normc,normd

norma=sqrt(norma)
normb=sqrt(normb)
normc=sqrt(normc)
normd=sqrt(normd)
write(*,*)norma,normb,normc,normd
end program computeNorm
