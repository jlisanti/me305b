#!/bin/gnuplot -persist
 set terminal postscript eps enhanced "Times,30" solid
 set output "fig.eps"
 set termoption dashed
 #set nokey
 set key outside
 set size 2.05,2.05
 set origin 0,0
 set multiplot
 set tmargin 0.6
 set bmargin 0.6
 set rmargin 0.0
 set lmargin 0.0
 #set autoscale
 set offsets graph 0.0, graph 0.0, graph 0.095, graph 0.025

 set xrange [0:1]
 
 set key top center
 set size 0.8,0.8
 set origin 0.15,1.125
 set title "Density" offset 0.0,0.0
 p "output.dat" u 1:2  notitle axes x1y1 w l lt 1 lc 1 lw 3,\
   "output.dat" u 1:8  title "Toro {/Symbol r}_*L" axes x1y1 w l lt 5 lc 7 lw 0.5,\
   "output.dat" u 1:9  title "Toro {/Symbol r}_*R" axes x1y1 w l lt 4 lc 7 lw 0.5 

 set key top right
 set size 0.8,0.8
 set origin 1.15,1.125
 set title "Velocity" offset 0.0,0.0
 p "output.dat" u 1:3 axes x1y1 w l lt 1 lc 1 lw 3 notitle,\
   "output.dat" u 1:7 axes x1y1 w l lt 4 lc 7 lw 0.5 title "Toro u_*"

 set size 0.8,0.8
 set origin 1.15,0.125
 set title "Internal Energy" offset 0.0,0.0
 p "output.dat" u 1:5 axes x1y1 w l lt 1 lc 1 lw 3 notitle

 set size 0.8,0.8
 set origin 0.15,0.125
 set title "Pressure" offset 0.0,0.0
 p "output.dat" u 1:4 axes x1y1 w l lt 1 lc 1 lw 3 notitle,\
   "output.dat" u 1:6 axes x1y1 w l lt 4 lc 7 lw 0.5 title "Toro p_*"
 
 unset multiplot
 reset 
