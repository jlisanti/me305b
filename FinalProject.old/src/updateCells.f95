SUBROUTINE updateCells(Uij,flux,dtij)
USE inputMod
use errorMod
IMPLICIT NONE

!integer,intent(in)::numi,numf
!integer,intent(in)::ind
!integer,intent(in)::dir
integer::n,l
REAL(dp),DIMENSION(numf+4,dim+2),INTENT(inout)::Uij
REAL(dp),DIMENSION(numf+4,dim+2),INTENT(in)::flux
REAL(dp),INTENT(in)::dtij
!real(dp)::rhotemp,utemp,vtemp,ptemp
!integer::ij
!integer::ind=20

!if((i==checki) .and. (k==checkt) .and. (dir==1)) then
!if((dir==1) .and. (i==checki)) then
!write(*,*)
!write(*,*)'--------------------------'
!write(*,*)'before update'
!do ij=numi-2,numf+2
!  rhotemp=Uij(ij,1)
!  utemp=Uij(ij,2)/rhotemp
!  vtemp=Uij(ij,3)/rhotemp
!  ptemp=(gamma-1.0_dp)*(Uij(ij,4)-0.5_dp&
!         *(Uij(ij,2)*(Uij(ij,2)/Uij(ij,1))&
!          +Uij(ij,3)*(Uij(ij,3)/Uij(ij,1))))
  !write(*,*)ind,i,ij,rhotemp,utemp,vtemp,ptemp
!end do
!end if
!if(dir==1) then
!  DO n=numi,numf
    !DO l=1,(dim+2)
      !U(i,n,1)=Uij(n,1)+(dtij/del)*(flux(n-1,1)-flux(n,1))
      !U(i,n,2)=Uij(n,3)+(dtij/del)*(flux(n-1,3)-flux(n,3))
      !U(i,n,3)=Uij(n,2)+(dtij/del)*(flux(n-1,2)-flux(n,2))
      !U(i,n,4)=Uij(n,4)+(dtij/del)*(flux(n-1,4)-flux(n,4))
      !U(i,n,1)=Uij(n,1)+(dtij/del)*(flux(n-1,1)-flux(n,1))
      !U(i,n,2)=Uij(n,3)+(dtij/del)*(flux(n-1,3)-flux(n,3))
      !U(i,n,3)=Uij(n,2)+(dtij/del)*(flux(n-1,2)-flux(n,2))
      !U(i,n,4)=Uij(n,4)+(dtij/del)*(flux(n-1,4)-flux(n,4))
      !if(ISNAN(U(i,n,l))) then
      !  error=126
      !  call errorF(error)
      !  write(*,*)'U ',n,'is now NAN'
      !  write(*,*)'flux i',l,'=',flux(n,l)
      !  write(*,*)'flux i-1',l,'=',flux(n-1,l)
      !  write(*,*)
        !stop
      !end if
    !END DO
!  END DO
!do l=1,(dim+2)
!  U(i,numi-2,1)=Uij(numi-2,1)
!  U(i,numi-1,2)=Uij(numi-1,3)
!  U(i,numf+1,3)=Uij(numf+1,2)
!  U(i,numf+2,4)=Uij(numf+2,4)
!  
!  U(i,numi-2,1)=Uij(numi-2,1)
!  U(i,numi-1,2)=Uij(numi-1,3)
!  U(i,numf+1,3)=Uij(numf+1,2)
!  U(i,numf+2,4)=Uij(numf+2,4)
!
!  U(i,numi-2,1)=Uij(numi-2,1)
!  U(i,numi-1,2)=Uij(numi-1,3)
!  U(i,numf+1,3)=Uij(numf+1,2)
!  U(i,numf+2,4)=Uij(numf+2,4)

!  U(i,numi-2,1)=Uij(numi-2,1)
!  U(i,numi-1,2)=Uij(numi-1,3)
!  U(i,numf+1,3)=Uij(numf+1,2)
!  U(i,numf+2,4)=Uij(numf+2,4)
!end do
!else
  DO n=numi,numf
    DO l=1,(dim+2)
      !U(n,j,l)=Uij(n,l)+(dtij/del)*(flux(n-1,l)-flux(n,l))
      Uij(n,l)=Uij(n,l)+(dtij/del)*(flux(n-1,l)-flux(n,l))
      if(ISNAN(Uij(n,l))) then
        error=126
        call errorF(error)
        write(*,*)'U ',l,'is now NAN'
        write(*,*)'flux i',l,'=',flux(n,l)
        write(*,*)'flux i-1',l,'=',flux(n-1,l)
        write(*,*)
        stop
      end if
    END DO
  END DO
!do l=1,(dim+2)
!  U(numi-2,j,l)=Uij(numi-2,l)
!  U(numi-1,j,l)=Uij(numi-1,l)
!  U(numf+1,j,l)=Uij(numf+1,l)
!  U(numf+2,j,l)=Uij(numf+2,l)
!end do
!end if

!if((i==checki) .and. (k==checkt) .and. (dir==1)) then
!if((dir==1) .and. (i==checki))then
!write(*,*)'after update'
!do ij=numi-2,numf+2
!  rhotemp=U(i,ij,1)
!  utemp=U(i,ij,2)/rhotemp
!  vtemp=U(i,ij,3)/rhotemp
!  ptemp=(gamma-1.0_dp)*(U(i,ij,4)-0.5_dp&
!         *(U(i,ij,2)*(U(i,ij,2)/U(i,ij,1))&
!          +U(i,ij,3)*(U(i,ij,3)/U(i,ij,1))))
  !write(*,*)ind,i,ij,rhotemp,utemp,vtemp,ptemp
!end do
!write(*,*)'-----------------------'
!write(*,*)
!stop
!end if

!____________________________________________________________!
!if(dir==1) then
!write(*,*)
! do ij=numi,numf
!  rhotemp=U(i,ij,1)
!  utemp=U(i,ij,2)/rhotemp
!  vtemp=U(i,ij,3)/rhotemp
!  ptemp=(gamma-1.0_dp)*(U(i,ij,4)-0.5_dp&
!         *(U(i,ij,2)*(U(i,ij,2)/U(i,ij,1))&
!          +U(i,ij,3)*(U(i,ij,3)/U(i,ij,1))))
!  if(ptemp<0.0_dp) then
!    error=200
!    call errorF(error)
!    write(*,*)'direction',dir
!    write(*,*)U(i,ij,1),U(i,ij,2),U(i,ij,3),U(i,ij,4)
!    write(*,*)rhotemp,utemp,vtemp,ptemp
!    write(*,*)i,ij,istep,jstep
!    stop
!  end if
! write(*,*)rhotemp,utemp,vtemp,ptemp
! end do
!write(*,*)
! else
!write(*,*)
! do ij=numi-2,numf+2
!  rhotemp=U(ij,j,1)
!  utemp=U(ij,j,2)/rhotemp
!  vtemp=U(ij,j,3)/rhotemp
!  ptemp=(gamma-1.0_dp)*(U(ij,j,4)-0.5_dp&
!         *(U(ij,j,2)*(U(ij,j,2)/U(ij,j,1))&
!          +U(ij,j,3)*(U(ij,j,3)/U(ij,j,1))))
!  if(ptemp<0.0_dp) then
!    error=200
!    call errorF(error)
!    write(*,*)'direction',dir
!    write(*,*)U(ij,j,1),U(ij,j,2),U(ij,j,3),U(ij,j,4)
!    write(*,*)rhotemp,utemp,vtemp,ptemp
!    write(*,*)j,ij,istep,jstep
!    stop
!  end if
!
! end do
!
!end if
!____________________________________________________!
  !write(*,*)ind,i,ij,rhotemp,utemp,vtemp,ptemp
!write(*,*)dir,'end of update'
!do n=numi-2,numf+2
!write(*,*)
!write(*,*)n,dir,Uij(n,1),Uij(n,2),Uij(n,3),Uij(n,4)
!if(dir==1) then
!write(*,*)n,dir,U(ind,n,1),U(ind,n,2),U(ind,n,3),U(ind,n,4)
!else
!write(*,*)n,dir,U(n,ind,1),U(n,ind,2),U(n,ind,3),U(n,ind,4)
!end if
!write(*,*)
!end do

END SUBROUTINE updateCells
