#!/bin/gnuplot -persist
reset
set term postscript enhanced color
set pointsize 1.5
set out "Temps.eps"

set xrange [0:3]
set yrange [0:1]
set isosample 30,30,30
set table 'test.dat'
splot "output.dat"
unset table

#set contour base
#set dgrid3d
#set cntrparam level incremental -3, 0.5, 3
#unset surface
#set table 'cont.dat'
#splot "output.dat"
#unset table

reset
set xrange [0:3]
set yrange [0:1]
unset key
set palette rgbformulae 33,13,10
#p 'test.dat' with image, 'cont.dat' w l lt -1 lw 1.5
p 'test.dat' w l lt -1 lw 1.5
