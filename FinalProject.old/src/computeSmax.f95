subroutine computeSmax(SxMax,SyMax)
use inputMod
use HLLCMod
implicit none
real(dp),intent(out)::SxMax,SyMax
real(dp)::c
real(dp)::S=0.0_dp
real(dp)rho,uvel,vvel,p
real(dp)::energy
!real(dp)::SR,Sstar,SL
SxMax=0.0_dp
SyMax=0.0_dp

do i=1,imax
  do j=1,jmax
    !rhoL = U(i,j,1)
    !uvelL = U(i,j,2)/U(i,j,1)
    !vvelL = U(i,j,3)/U(i,j,1)
    !energy = 0.5_dp*(U(i,j,2)*uvelL+U(i,j,3)*vvelL)
    !pL = (gamma-1.0_dp)*(U(i,j,4)-energy)
    !call soundSpeed(rhoL,pL,gamma,aL)

    !rhoR = U(i,j,1)
    !uvelR = U(i,j,2)/U(i,j,1)
    !vvelR = U(i,j,3)/U(i,j,1)
    !energy = 0.5_dp*(U(i,j,2)*uvelR+U(i,j,3)*vvelR)
    !pR = (gamma-1.0_dp)*(U(i,j,4)-energy)
    !call soundSpeed(rhoR,pR,gamma,aR)
    
    !call estimateWaveSpeeds(SL,Sstar,SR)
    rho = U(i,j,1)
    uvel = U(i,j,2)/U(i,j,1)
    vvel = U(i,j,3)/U(i,j,1)
    energy = 0.5_dp*(U(i,j,2)*uvel+U(i,j,3)*vvel)
    p = (gamma-1.0_dp)*(U(i,j,4)-energy)
    call soundSpeed(rho,p,gamma,c)    

    S=ABS(uvel)+c
    if(S>SxMax) then
      SxMax=S
    end if
    S=ABS(vvel)+c
    if(S>SyMax) then
      SyMax=S
    end if
  end do
end do

end subroutine computeSmax
