program twoDEuler
use inputMod
implicit none

! Data dictionary

! Read input file

! Generate mesh
call mesh

! Initialize domain
call initializeDomain

write(*,*)'initial conditions'
write(*,*)'density',U(10,10,1)
write(*,*)'x1 velocity',U(10,10,2)/U(10,10,1)
write(*,*)'x2 velocity',U(10,10,3)/U(10,10,1)
write(*,*)'pressure',(gamma-1.0_dp)*(U(10,10,4)-0.5_dp&
                      *(U(10,10,2)*(U(10,10,2)/U(10,10,1))&
                      +U(10,10,3)*(U(10,10,3)/U(10,10,1))))

prim(1)=0.0_dp
prim(2)=10.0_dp
prim(3)=0.0_dp
prim(4)=10.0_dp
prim(5)=0.0_dp
prim(6)=10.0_dp
prim(7)=0.0_dp
prim(8)=10.0_dp

!call writeSolution
!stop
! Advance solution in time
do k=1,maxiter
!write(*,*)'made it in time loop'
  ! Impose CFL
  call imposeCFL 
  !write(*,*)'imposed CFL',dt,t
  ! Step time forward
  t = t + dt
  twrite=twrite+dt
  ! Advance Solution
  call advanceSolution
  ! Check if output time is reached
  if(t>=tout) then 
    ! Write solution to file 
    write(*,*)'time= ',t
    write(*,*)'Output time reached, exiting...'
    writei=8
    call writeSolution 
    exit
  end if

  !write(*,*)'advanced solution' 
  if(counter==200) then 
    write(*,*)
    write(*,100)'Iteration',k,'t=',t,' dt=',dt
    write(*,110)'Max rho ',prim(1),'Min rho',prim(2)
    write(*,110)'Max p   ',prim(7),'Min p  ',prim(8)
    counter=0
    100 format (A10,I5,A3,F5.3,A4,ES10.3)
    110 format (A12,F5.2,A12,F5.2)
    write(*,*)
  end if
  counter=counter+1 
  if(twrite>0.5) then
    call writeSolution
    twrite=0.0_dp
    writei=writei+1
  end if
end do
end program twoDEuler
