subroutine initializeDomain
use inputMod
implicit none

! Fill domain prior to step
do i=1,istep
  do j=1,(jmax)
    U(i,j,1)=rhoi
    U(i,j,2)=rhoi*veli
    U(i,j,3)=0.0_dp
    U(i,j,4)=rhoi*(0.5_dp*(veli*veli))+pi/(gamma-1.0_dp)
  end do
end do

! Fill domain aft of step
do i=istep,(imax)
  do j=jstep,(jmax)
    U(i,j,1)=rhoi
    U(i,j,2)=rhoi*veli
    U(i,j,3)=0.0_dp
    U(i,j,4)=rhoi*(0.5_dp*(veli*veli))+pi/(gamma-1.0_dp)
  end do
end do
end subroutine initializeDomain
