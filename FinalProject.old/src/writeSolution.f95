subroutine writeSolution
use inputMod
real(dp)::rho,v1,v2,p,A
integer::n,l
if(writei==1) then
  open(unit=2,file='output_t05.dat',status='replace',action='write')
elseif(writei==2) then
  open(unit=3,file='output_t10.dat',status='replace',action='write')
elseif(writei==3)  then
  open(unit=4,file='output_t15.dat',status='replace',action='write')
elseif(writei==4)  then
  open(unit=5,file='output_t20.dat',status='replace',action='write')
elseif(writei==5)  then
  open(unit=6,file='output_t25.dat',status='replace',action='write')
elseif(writei==6)  then
  open(unit=7,file='output_t30.dat',status='replace',action='write')
elseif(writei==7)  then
  open(unit=8,file='output_t35.dat',status='replace',action='write')
elseif(writei==10)  then
  write(*,*)'Solution dumped to output_dump.dat'
  open(unit=11,file='output_dump.dat',status='replace',action='write')
else
  open(unit=9,file='output_t40.dat',status='replace',action='write')
end if
rewind(writei+1)
do n=1,imax
  do l=1,(jmax)
    rho=U(n,l,1)
    v1=U(n,l,2)/rho
    v2=U(n,l,3)/rho
    p=(gamma-1.0_dp)*(U(n,l,4)-0.5_dp&
                      *(U(n,l,2)*(U(n,l,2)/U(n,l,1))&
                      +U(n,l,3)*(U(n,l,3)/U(n,l,1))))
    A=p/(rho**gamma)
    if((n>istep) .and. (l<jstep)) then
       write(writei+1,*)x(n,l),y(n,l),0.0_dp,0.0_dp,0.0_dp,0.0_dp,0.0_dp
    else
      write(writei+1,*)x(n,l),y(n,l),rho,v1,v2,p,A
    end if
    if(l==jmax) then
      write(writei+1,*)
    end if
  end do
end do

!! Fill domain aft of step
!do n=istep,(imax)
!  do l=jstep,(jmax)
!    !write(2,*)x(n,l),y(n,l),U(n,l,1),U(n,l,2)/U(n,l,1),U(n,l,3)/U(n,l,1)
!    rho=U(n,l,1)
!    v1=U(n,l,2)/rho
 !   v2=U(n,l,3)/rho
!    p=(gamma-1.0_dp)*(U(n,l,4)-0.5_dp&
!                      *(U(n,l,2)*(U(n,l,2)/U(n,l,1))&
!                      +U(n,l,3)*(U(n,l,3)/U(n,l,1))))
!    A=p/(rho**gamma)
!    write(2,*)x(n,l),y(n,l),rho,v1,v2,p,A
!    if(l==jmax) then
!      write(2,*)
!    end if
!  end do
!end do
close(writei+1)
end subroutine
