module inputMod
implicit none

! data dictionary
integer, parameter :: dp = selected_real_kind(15, 307)
real(dp)::gamma=1.4_dp
real(dp)::t,tout,dt
real(dp)::del!dx,dy,del
real(dp)::twrite
real(dp),allocatable,dimension(:,:)::x,y
real(dp),allocatable,dimension(:,:,:)::U
real(dp)::rhoi,veli,pi
real(dp),dimension(8)::prim
integer::counter
integer::dir
integer::numi,numf
integer::maxiter=100000000
integer::dim=2
integer::checkt=2
integer::checki=20
integer::k
integer::writei=1
integer::i,j
integer::imax,jmax
integer::istep,jstep
character(100)::file_name

end module inputMod
