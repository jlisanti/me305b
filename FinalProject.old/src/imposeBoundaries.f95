subroutine imposeBoundaries(Uij,bound)
use inputMod
implicit none
real(dp),dimension(numf+4,4),intent(inout)::Uij
integer,dimension(2),intent(in)::bound

! Forward boundary
Uij(numi-2,1)=Uij(numi+1,1)
Uij(numi-1,1)=Uij(numi,1)
Uij(numi-2,3)=Uij(numi+1,3)
Uij(numi-1,3)=Uij(numi,3)
Uij(numi-2,4)=Uij(numi+1,4)
Uij(numi-1,4)=Uij(numi,4)

! Aft boundary
Uij(numf+2,1)=Uij(numf-1,1)
Uij(numf+1,1)=Uij(numf,1)
Uij(numf+2,3)=Uij(numf-1,3)
Uij(numf+1,3)=Uij(numf,3)
Uij(numf+2,4)=Uij(numf-1,4)
Uij(numf+1,4)=Uij(numf,4)

! Set forward boundary
if(bound(1)==1) then

   ! Inlet conditions
   Uij(numi-2,1)=rhoi
   Uij(numi-1,1)=rhoi
   Uij(numi-2,2)=rhoi*veli
   Uij(numi-1,2)=rhoi*veli
   Uij(numi-2,3)=0.0_dp
   Uij(numi-1,3)=0.0_dp
   Uij(numi-2,4)=0.5_dp*rhoi*(veli*veli)+pi/(gamma-1.0_dp)
   Uij(numi-1,4)=0.5_dp*rhoi*(veli*veli)+pi/(gamma-1.0_dp)
elseif(bound(1)==2) then
   ! Transmissive
   Uij(numi-2,2)=Uij(numi+1,2)
   Uij(numi-1,2)=Uij(numi,2)
else
   ! Reflective
   Uij(numi-2,2)=Uij(numi+1,2)
   Uij(numi-2,2)=Uij(numi,2)
end if

! Set aft boundary
if(bound(2)==2) then
   if(dir==1) then 
     write(*,*) 'WHAT'
   end if
   ! Transmissive
   Uij(numf+2,2)=Uij(numf-1,2)
   Uij(numf+1,2)=Uij(numf,2)
else
   ! Reflective
   Uij(numf+2,2)=-Uij(numf-1,2)
   Uij(numf+1,2)=-Uij(numf,2)
end if
end subroutine imposeBoundaries
