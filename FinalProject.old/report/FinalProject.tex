%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage{graphicx} % Required to insert images
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{amssymb}
\usepackage[]{units}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow}
%\usepackage[]{hyperref}
\usepackage[]{natbib}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{mathtools}
\usepackage{amsmath}

\usepackage[includeheadfoot]{geometry}
\geometry{hmargin=3cm,vmargin=3.5cm}

\usepackage{fancyhdr}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.25in
\headsep=0.2in 

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass\ \hmwkTitle} % Top center header
\rhead{\hmwkDueDate}
%\rhead{\firstxmark} % Top right header
%\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

%----------------------------------------------------------------------------------------
%	NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{Final Project} % Assignment title
\newcommand{\hmwkDueDate}{Thursday\ July\ 25,\ 2013} % Due date
\newcommand{\hmwkClass}{ME\ 305B} % Course/class
\newcommand{\hmwkClassTime}{10:30am} % Class/lecture time
\newcommand{\hmwkClassInstructor}{Bisetti} % Teacher/lecturer
\newcommand{\hmwkAuthorName}{Joel Lisanti} % Your name


\begin{document}

{ \large Introduction}\\



The following are the nondimensionalized Navier-Stokes and continuity equations for incompressible viscous flows in two dimensions.  These equations will be discretized and solved numerically using the fractional step method.  Note that density will be taken to be equal to unity and that only uniform mesh spacing will be considered in the $x_{1}$ and $x_{2}$ directions.
\\

$x_{1}$-Momentum
\begin{equation}
	\frac{\partial u_{1}}{\partial t}+(u_{1}\frac{\partial u_{1}}{\partial x_{1}}+u_{2}\frac{\partial u_{1}}{\partial x_{2}})=-\frac{1}{\rho}\frac{\partial p}{\partial x_{1}}+\frac{1}{Re}(\frac{\partial^{2}}{\partial x_{1}^{2}}+\frac{\partial^{2}}{\partial x_{2}^{2}})u_{1}
\end{equation}

$x_{2}$-Momentum
\begin{equation}
	\frac{\partial u_{2}}{\partial t}+(u_{1}\frac{\partial u_{2}}{\partial x_{1}}+u_{2}\frac{\partial u_{2}}{\partial x_{2}})=-\frac{1}{\rho}\frac{\partial p}{\partial x_{2}}+\frac{1}{Re}(\frac{\partial^{2}}{\partial x_{1}^{2}}+\frac{\partial^{2}}{\partial x_{2}^{2}})u_{2}
\end{equation}

Continuity
\begin{equation}
	\frac{\partial u_{1}}{\partial x_{1}}+\frac{\partial u_{2}}{\partial x_{2}}=0
\end{equation}

Also, note that pressure is defined here as:
\begin{equation}
	p=\phi+(\Delta t/2Re)\nabla^{2}\phi
\end{equation}

{\large Book Keeping}\\
\begin{figure}[h]
 \centering
   \includegraphics[width=0.9\textwidth]{bookkeeping.png}
       \caption{Staggered Grid Arrangement}
\end{figure}
\\[1.5cm]

%Note the above figure details the staggered grid used to discretize the governing equations.


{\large Grid}\\
\begin{figure}[h]
 \centering
   \includegraphics[width=0.7\textwidth]{Grid.png}
       \caption{Cavity Driven Flow, Grid}
\end{figure}


{ \large Time Advancement}\\

The following steps are the required high level operations required to advance the solution in time.
\\

Step (1) : Advance velocity field in time using equation (5)
\begin{equation}
	\hat{u}_{i}=\Delta t\left[\frac{1}{2}(3H_{i}^{n}-H_{i}^{n-1})+\frac{1}{2}\frac{1}{Re}(\frac{\delta^{2}}{\delta x_{1}^{2}}+\frac{\delta^{2}}{\delta x_{2}^{2}})(\hat{u}_{2}+u_{2}^{n})\right]+u_{i}^{n}
\end{equation}
Step (2) : Compute $\phi^{n+1}$ through solution to pressure equation (6)
\begin{equation}
	(\frac{\delta^{2}}{\delta x_{1}^{2}}+\frac{\delta^{2}}{\delta x_{2}^{2}})\phi^{n+1}(i,j)=\frac{1}{\Delta t}\left(\frac{\delta \hat{u}_{1}}{\delta x_{1}}+\frac{\delta \hat{u}_{2}}{\delta x_{2}}\right)
\end{equation}
Step (3) : Update  velocities to divergence free values, equation (7)
\begin{equation}
	u_{i}^{n+1}=-\Delta t\left(\frac{\delta}{\delta x_{1}}+\frac{\delta}{\delta x_{2}}\right)\phi^{n+1}+\hat{u}_{i}
\end{equation}
\\
\\[1.5cm]

{ \large Discrete Spatial Operators}\\

Convective Terms
\\

$x_{1}$-Momentum
\begin{equation}
\begin{multlined}
	H_{1}=\frac{u_{1}^{2}(i-1,j)-u_{1}^{2}(i+1,j)}{2h_{1}}+\frac{1}{h_{2}}[\frac{(u_{1}(i,j+1)+u_{1}(i,j))}{2}\frac{(u_{2}(i-1,j+1)+u_{2}(i,j+1))}{2}\\
	-\frac{(u_{1}(i,j-1)+u_{1}(i,j))}{2}\frac{(u_{2}(i-1,j)+u_{2}(i,j))}{2}]
\end{multlined}
\end{equation}
$x_{2}$-Momentum
\begin{equation}
\begin{multlined}
	H_{2}=\frac{u_{2}^{2}(i-1,j)-u_{2}^{2}(i+1,j)}{2h_{2}}+\frac{1}{h_{1}}[\frac{(u_{2}(i,j+1)+u_{2}(i,j))}{2}\frac{(u_{1}(i-1,j+1)+u_{1}(i,j+1))}{2}\\
	-\frac{(u_{2}(i,j-1)+u_{2}(i,j))}{2}\frac{(u_{1}(i-1,j)+u_{1}(i,j))}{2}]
\end{multlined}
\end{equation}

Diffusive Terms
\\

Define the following value to simplify the notation of the discretized diffusive term
\begin{equation}
u_{i}^{*}=(\hat{u}_{i}+u_{i}^{n})
\end{equation}
$x_{1}$-Momentum
\begin{equation}
\begin{multlined}
	(\frac{\delta^{2}}{\delta x_{1}^{2}}+\frac{\delta^{2}}{\delta x_{2}^{2}})u_{1}^{*}
	=\left[\frac{u_{1}^{*}(i+1,j)-u_{1}^{*}(i,j)}{h_{1}}-\frac{u_{1}^{*}(i,j)-u_{1}^{*}(i-1,j)}{h_{1}}\right]\frac{1}{h_{1}}\\
	+\left[\frac{u_{1}^{*}(i,j+1)-u_{1}^{*}(i,j)}{h_{2}}-\frac{u_{1}^{*}(i,j)-u_{1}^{*}(i,j-1)}{h_{2}}\right]\frac{1}{h_{2}}
\end{multlined}
\end{equation}

$x_{2}$-Momentum
\begin{equation}
\begin{multlined}
	(\frac{\delta^{2}}{\delta x_{1}^{2}}+\frac{\delta^{2}}{\delta x_{2}^{2}})u_{2}^{*}
	=\left[\frac{u_{2}^{*}(i+1,j)-u_{2}^{*}(i,j)}{h_{1}}-\frac{u_{2}^{*}(i,j)	-u_{2}^{*}(i-1,j)}{h_{1}}\right]\frac{1}{h_{x}}\\
	+\left[\frac{u_{2}^{*}(i,j+1)-u_{2}^{*}(i,j)}{h_{2}}-\frac{u_{1}^{*}(i,j)-u_{2}^{*}(i,j-1)}{h_{2}}\right]\frac{1}{h_{2}}
\end{multlined}
\end{equation}

Pressure
\begin{equation}
	\frac{\delta}{\delta x_{1}}\phi^{n+1}=\frac{\phi^{n+1}(i-1,j)-\phi^{n+1}(i,j)}{h_{1}}
\end{equation}

\begin{equation}
	\frac{\delta^{2}}{\delta x_{1}^{2}}\phi^{n+1}=\left[\frac{\phi^{n+1}(i+1,j)-\phi^{n+1}(i,j)}{h_{1}}-\frac{\phi^{n+1}-\phi^{n+1}(i-1,j)}{h_{1}}\right]\frac{1}{h_{1}}
\end{equation}

\begin{equation}
	\frac{\delta}{\delta x_{2}}\phi^{n+1}=\frac{\phi^{n+1}(i,j-1)-\phi^{n+1}(i,j)}{h_{2}}
\end{equation}

\begin{equation}
	\frac{\delta^{2}}{\delta x_{2}^{2}}\phi^{n+1}=\left[\frac{\phi^{n+1}(i,j+1)-\phi^{n+1}(i,j)}{h_{2}}-\frac{\phi^{n+1}-\phi^{n+1}(i,j-1)}{h_{2}}\right]\frac{1}{h_{2}}
\end{equation}
\\[1.5cm]
\\[1.5cm]

{ \large Boundary Conditions}\\

In order to close the pressure equation it is necessary to have an intermediate boundary condition on $\hat{u}_{i}$.  The following equation has been shown to be an effective derived boundary condition for this purpose, Kim and Moin [1].
\begin{equation}
	\hat{u}_{i}=u_{i}^{n+1}+\Delta t\frac{\partial \phi^{n}}{\partial x_{i}}
\end{equation}
Boundary conditions for the velocity of nodes lying on the boundary ($u_{i}^{n+1}$) would be set as follows:
For stationary wall
\begin{equation}
	u_{1}^{n+1}=0
\end{equation}

\begin{equation}
	u_{2}^{n+1}=0
\end{equation}

For the moving wall ($x_{1}$ normal to the wall)
\begin{equation}
	u_{1}^{n+1}=0
\end{equation}

\begin{equation}
	u_{2}^{n+1}=\mbox{speed of moving wall}
\end{equation}
\\
The velocity at nodes adjacent to the walls do not always have a defined component on the wall.  This requires that ghost cells be introduced and for the necessary information to be interpolated. 
\\

Velocity components in the corners are also undefined and will, in the same way, require ghost cells and interpolation. There is added complication when the corner is adjacent to the moving wall in which the velocity interpolation may require two different values depending on the direction.
\\

Note:
For the staggered grid arrangement, no boundary condition is required for pressure.\\


{ \large Validation}\\

For the purpose of validating that the method has been correctly implemented, the results of the cavity driven flow problem will be directly compared with previous results detailed in the literature, Kim and Moin [1]. These comparisons will be made with two dimensional streamline and velocity vector plots at Reynolds numbers of 1, 100, 400, 1000, 2000, and 5000.  Additionally, for direct comparison the profile of stream-wise velocity at the mid plane of the cavity will be plotted for Reynolds number = 400.  
\\

The streamline and velocity vector plots will allow the qualitative, physical features of the solution to be assessed and directly compared.  A stream-wise velocity profile will lend to direct comparison of quantitative numerical results.
\\

[1] J. Kim, P. Moin J. Comput. Phys., 59 (1985), p. 308

\end{document}
