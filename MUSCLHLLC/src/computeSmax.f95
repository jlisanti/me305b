SUBROUTINE computeSmax(U,Smax)
USE inputMod
IMPLICIT NONE

REAL(dp),DIMENSION(imax,3),INTENT(in)::U
REAL(dp),INTENT(out)::Smax
REAL(dp)::c
REAL(dp)::S=0.0_dp
REAL(dp)::density,velocity,pressure

DO i=2,imax-1
  density = U(i,1)
  velocity = U(i,2)/U(i,1)
  pressure = (gamma-1.0_dp)*(U(i,3)-0.5_dp*density*velocity*velocity)
  CALL soundSpeed(density,pressure,gamma,c)
  S=ABS(velocity)+c
  IF(S>Smax) THEN
    Smax=S
  END IF  
END DO
END SUBROUTINE computeSmax
