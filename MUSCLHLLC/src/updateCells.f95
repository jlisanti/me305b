SUBROUTINE updateCells(U,flux,dt)
USE inputMod
use errorMod
IMPLICIT NONE

REAL(dp),DIMENSION(imax,3),INTENT(inout)::U
REAL(dp),DIMENSION(imax,3),INTENT(in)::flux
REAL(dp),INTENT(in)::dt
integer::n
DO i=3,(imax-2)
  DO n=1,3
    U(i,n)=U(i,n)+(dt/dx)*(flux(i-1,n)-flux(i,n))
    if(ISNAN(U(i,n))) then
      error=126
      call errorF(error)
      write(*,*)'U ',n,'is now NAN'
      write(*,*)'flux i',n,'=',flux(i,n)
      write(*,*)'flux i-1',n,'=',flux(i-1,n)
      write(*,*)
      stop
    end if
  END DO
END DO

END SUBROUTINE updateCells
