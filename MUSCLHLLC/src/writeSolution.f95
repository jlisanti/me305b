SUBROUTINE writeSolution(U)
USE inputMod
IMPLICIT NONE

REAL(dp),DIMENSION(imax,3),INTENT(in)::U
REAL(dp)::rho,vel,pres,e

OPEN(UNIT=5,FILE='output.dat',STATUS='replace')
REWIND(5)
DO i=1,imax
  rho=U(i,1)
  vel=U(i,2)/U(i,1)
  pres=(gamma-1.0_dp)*(U(i,3)-0.5_dp*U(i,2)*vel)
  e=pres/(rho*(gamma-1.0_dp))
  WRITE(5,*)(dx*(REAL(i)-1.0_dp)),rho,vel,pres,e
END DO
CLOSE(5)
END SUBROUTINE writeSolution
