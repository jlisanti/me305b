!---------------------------------------------!
!  Impose transmissive boundaries for the 
!   1D shock tube problem
!---------------------------------------------!
SUBROUTINE imposeBoundaries(U)
USE inputMod
IMPLICIT NONE
REAL(dp),DIMENSION(imax,3),INTENT(inout)::U
integer::n

DO n=1,3
  U(1,n)=U(4,n)
  U(2,n)=U(3,n)
  U(imax,n)=U(imax-3,n)
  U(imax-1,n)=U(imax-2,n)
END DO

END SUBROUTINE imposeBoundaries
