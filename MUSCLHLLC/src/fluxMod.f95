module fluxMod
use inputMod 
implicit none 
contains
  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  !write(*,*)'made it to compute numerical flux'
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U3=rho*(0.5_dp*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux

  subroutine vanLeer(r,omega,deli)
  use inputMod
  implicit none
  real(dp),intent(inout) :: deli
  real(dp),intent(in) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xivl,xiR
  !write(*,*)'made it to valear'  
  xivl=0.0_dp  
  if(r<0.0_dp) then
    xivl=0.0_dp
  else
    xiR=2.0_dp/(1.0_dp-omega+(1.0_dp+omega)*r)
    xivl=min((2.0_dp*r)/(1.0_dp+r),xiR)
  end if
  deli=deli*xivl
  end subroutine vanLeer
  
  subroutine superbee(r,omega,deli)
  use inputMod
  implicit none
  real(dp),intent(inout) :: deli
  real(dp),intent(in) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xi,xiR,temp
  xi = 0.0_dp
  if(r>0.0_dp) then
    xi=2.0_dp*r
  end if
  if(r>0.5_dp) then
    xi=1.0_dp
  end if
  if(r>1.0_dp) then
    temp=1.0_dp-omega+(1.0_dp+omega)*r
    xiR=2.0_dp/temp
    xi=MIN(xiR,r)
    xi=MIN(xi,2.0_dp)
  end if
  deli=deli*xi
  end subroutine superbee
end module fluxMod
