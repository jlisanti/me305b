!------------------------------------------------------------------!
!  The purpose of this subroutine is to compute numerical
!    flux using the HLLC approximate solution to the
!    Riemann problem.
!------------------------------------------------------------------!
subroutine computeHLLCflux(WL,WR,flux)
use inputMod
use errorMod
use HLLCMod
implicit none
real(dp),dimension(3),intent(in)::WL,WR
real(dp),dimension(3),intent(out)::flux
real(dp),dimension(3)::numericalFlux
real(dp),dimension(3)::Ustar,Ul,Ur
real(dp)::SL,Sstar,SR
integer::n,ca
rhoL=WL(1)
velL=WL(2)
pL=WL(3)

rhoR=WR(1)
velR=WR(2)
pR=WR(3)

call soundSpeed(rhoL,pL,gamma,aL)
call soundSpeed(rhoR,pR,gamma,aR)
call estimateWaveSpeeds(SL,Sstar,SR)
 
if(SL>0.0_dp) then
  call computeNumericalFlux(rhoL,velL,pL,numericalFlux)
  do n=1,3
    flux(n)=numericalFlux(n)
  end do
  ca=1
end if
  
if((SL<0.0_dp) .and. (SR>0.0_dp)) then
  if(Sstar>0.0_dp) then
    call computeNumericalFlux(rhoL,velL,pL,numericalFlux)
    call computeU(rhoL,velL,pL,Ul)
    call computeUstar(Sstar,SL,rhoL,velL,pL,Ustar)
    do n=1,3
      flux(n)=numericalFlux(n)+SL*(Ustar(n)-Ul(n))
    end do
    ca=2
  else
    call computeNumericalFlux(rhoR,velR,pR,numericalFlux)
    call computeU(rhoR,velR,pR,Ur)
    call computeUstar(Sstar,SR,rhoR,velR,pR,Ustar)
    do n=1,3
      flux(n)=numericalFlux(n)+SR*(Ustar(n)-Ur(n))
    end do
    ca=3
  end if
end if
  
if(SR<0.0_dp) then
 call computeNumericalFlux(rhoR,velR,pR,numericalFlux)
  do n=1,3
    flux(n)=numericalFlux(n)
  end do
  ca=4
end if

 do n=1,3
  if(ISNAN(flux(n))) then
    error=128
    call errorF(error)
    write(*,*)'fluxi ',n,'is now NAN'
    write(*,*)'flux',i,n,'=',flux(n)
    write(*,*)'inputs:'
    write(*,*)'left state',rhoL,velL,pL
    write(*,*)'right state',rhoR,velR,pR
    write(*,*)'case ',ca
    write(*,*)'numericalFlux',numericalFlux(n)
    write(*,*)'SR',SR
    write(*,*)'Ustar',Ustar(n)
    write(*,*)'Ur',Ur(n)
    write(*,*)
    stop
  end if
end do

end subroutine computeHLLCflux
