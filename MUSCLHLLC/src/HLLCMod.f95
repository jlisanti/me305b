module HLLCMod
use inputMod 
implicit none 
real(dp)::rhoL,velL,pL
real(dp)::rhoR,velR,pR
real(dp)::aL,aR

contains
  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U3=rho*(0.5_dp*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux

  subroutine computeU(rho,vel,p,U)
  use inputMod
  implicit none
  real(dp),intent(in)::rho,vel,p
  real(dp),dimension(3),intent(out)::U
  real(dp)::specificEnergy
  U(1)=rho
  U(2)=rho*vel
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U(3)=rho*(0.5_dp*vel*vel+specificEnergy)
  end subroutine computeU

  subroutine computeUStar(Sstar,Sk,rho,vel,p,Ustar)
  use inputMod
  implicit none
  real(dp),intent(in)::Sstar,Sk,rho,vel,p
  real(dp),dimension(3),intent(out)::Ustar
  real(dp)::A,B,C
  real(dp)::se
  se=p/((gamma-1.0_dp)*rho)
  A=rho*((Sk-vel)/(Sk-Sstar))
  B=(0.5_dp*((vel*vel))+se)
  C=Sstar+(p/(rho*(Sk-vel)))
  Ustar(1)=A
  Ustar(2)=A*Sstar
  Ustar(3)=A*(B+(Sstar-vel)*C)
  end subroutine computeUStar

  subroutine estimateWaveSpeeds(SL,Sstar,SR)
  use inputMod
  use errorMod
  implicit none

  ! Data dictionary
  real(dp),intent(out)::SL,Sstar,SR
  REAL(dp) :: c1,c2,c3,c4,c5,c6,c7,c8
  REAL(dp) :: gL,gR
  REAL(dp) :: pMin,pMax,qMax
  REAL(dp) :: ppv
  real(dp) ::cup
  real(dp) ::pq,PTL,PTR
  real(dp) :: po
  real(dp) ::um
  integer::ca
  c1 = (gamma-1.0_dp)/(2.0_dp*gamma)
  c2 = (gamma+1.0_dp)/(2.0_dp*gamma)
  c3 = 2.0_dp*gamma/(gamma-1.0)
  c4 = 2.0_dp/(gamma-1.0_dp)
  c5 = 2.0_dp/(gamma+1.0_dp)
  c6 = (gamma-1.0_dp)/(gamma+1.0_dp)
  c7 = (gamma-1.0_dp)/2.0_dp
  c8 = gamma-1.0_dp

  ! Compute necessary quantities
  pMin = MIN(pL,pR)
  pMax = MIN(pL,pR)
  qMax = pMax/pMin
  cup=0.25_dp*(rhoL+rhoR)+(aL+aR)
  ppv=0.5_dp*(pL+pR)-0.5_dp*(velL-velR)*cup
  
  if(ISNAN(qMax)) then
    error=102
    call errorF(error)
  end if

  IF((qMax<2.0_dp).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
    po = ppv
    um=0.5_dp*(velL+velR)+0.5_dp*(pL-pR)/cup
    ca=1
  ELSEIF(ppv<pMin) THEN
    ! Two-Rarefaction 
    pq  = (pL/pR)**c1
    um  = (PQ*velL/aL + velR/aR + c4*(PQ - 1.0_dp))/(PQ/aL + 1.0_dp/aR)
    PTL = 1.0_dp + c7*(velL - um)/aL
    PTR = 1.0_dp + c7*(um - velR)/aR
    po  = 0.5_dp*(pL*PTL**c3 + pR*PTR**c3)
    ca=2
  ELSE
    ! Two-Shock
    gL=SQRT(c5/(ppv+c6*pL))
    gR=SQRT(c5/(ppv+c6*pR))
    po=(gL*pL+gR*pR-(velR-velL))/(gL+gR)
    um=0.5_dp*(velL+velR)+0.5_dp*(gR*(po-pR)-gL*(po-pL))
    ca=3
  END IF

  if(po<pL) then
    SL = velL-aL
  else
    SL = velL-aL*SQRT(1.0_dp+c2*(po/pL-1.0_dp))
  endif
  Sstar=um
  if(po<pR) then
    SR = velR+aR
  else
    SR = velR+aR*SQRT(1.0_dp+c2*(po/pR-1.0_dp))
  endif
  end subroutine estimateWaveSpeeds
end module HLLCMod
