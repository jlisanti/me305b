subroutine computeFlux(U,flux,dt)
use inputMod
use fluxMod
use errorMod
implicit none
real(dp) :: omega
real(dp),dimension(2) :: delU
real(dp),dimension(imax,i) :: UbarL,UbarR
real(dp),dimension(3) :: UL,UR,FL,FR
real(dp),dimension(imax,3), intent(in) :: U
real(dp),dimension(imax,3), intent(out) :: flux
real(dp),intent(in) :: dt
real(dp),dimension(3) :: fluxi
real(dp) :: deli
real(dp),dimension(3) :: WL,WR
real(dp) :: tol
real(dp) :: r
integer::l,n
do i=2,imax-1
  omega=0.0_dp
  tol=1.0e-5_dp
  do n=1,3
    delU(1)=U(i,n)-U(i-1,n)
    delU(2)=U(i+1,n)-U(i,n)
    do l=1,2
      if(ABS(delU(l)) < tol) then
        delU(l)=tol*SIGN(1.0_dp,delU(l))
      end if
    end do
    r=delU(1)/delU(2)
    deli=0.5_dp*(1.0_dp+omega)*delU(1)&
             +0.5_dp*(1.0_dp-omega)*delU(2)
    !write(*,*)i,r,delU(1),delU(2),deli,U(i+1,n),U(i,n)
    !write(*,*) 'unlimited',deli
    call vanLeer(r,omega,deli)
    !call superbee(r,omega,deli) 
    !write(*,*) 'limited',deli
    !deli=0.0_dp
    UL(n)=U(i,n)-0.5_dp*deli
    UR(n)=U(i,n)+0.5_dp*deli
  end do
  !write(*,*)UL(1),UR(1),UL(2),UR(2),UL(3),UR(3)
  !write(*,*)U(i+1,1),UR(1),U(i+1,2),UR(2),U(i,3),UL(3)
  ! Left state
  WL(1)=UL(1)
  WL(2)=UL(2)/UL(1)
  WL(3)=(gamma-1.0_dp)*(UL(3)-0.5_dp*UL(2)*WL(2))
  ! Right state
  WR(1)=UR(1)
  WR(2)=UR(2)/UR(1)
  WR(3)=(gamma-1.0_dp)*(UR(3)-0.5_dp*UR(2)*WR(2))
  call computeNumericalFlux(WL(1),WL(2),WL(3),FL)
  call computeNumericalFlux(WR(1),WR(2),WR(3),FR)
  !write(*,*)FL(1),FR(1),FL(2),FR(2),FL(3),FR(3)
  do n=1,3
    UbarR(i,n) = UL(n) + 0.5_dp*(dt/dx)*(FL(n)-FR(n))
    UbarL(i,n) = UR(n) + 0.5_dp*(dt/dx)*(FL(n)-FR(n))
  end do
end do

do i=2,imax-2
  !write(*,*)UbarL(1),UbarR(1),UbarL(2),UbarR(2),UbarL(3),UbarR(3)
  ! Solve local Riemann problem using HLLC approximate method
  ! Left state
  !write(*,*)U(i+1,1),UbarR(1),U(i+1,2),UbarR(2),U(i+1,3),UbarR(3)
  WL(1)=UbarL(i,1)
  WL(2)=UbarL(i,2)/UbarL(i,1)
  WL(3)=(gamma-1.0_dp)*(UbarL(i,3)-0.5_dp*UbarL(i,2)*WL(2))
  !  Right state
  WR(1)=UbarR(i+1,1)
  WR(2)=UbarR(i+1,2)/UbarR(i+1,1)
  WR(3)=(gamma-1.0_dp)*(UbarR(i+1,3)-0.5_dp*UbarR(i+1,2)*WR(2))
  call computeHLLCflux(WL,WR,fluxi)
  do n=1,3
    flux(i,n)=fluxi(n)
    if(ISNAN(flux(i,n))) then
      error=127
      call errorF(error)
      write(*,*)'fluxi ',n,'is now NAN'
      write(*,*)'flux',i,n,'=',flux(i,n)
      write(*,*)'inputs:'
      write(*,*)'left state',WL(1),WL(2),WL(3)
      write(*,*)'right state',WR(1),WR(2),WR(3)
      write(*,*)
      stop
    end if
  end do
  !write(*,*)i,fluxi(1),fluxi(2),fluxi(3)
end do
end subroutine computeFlux
