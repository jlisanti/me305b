module musclHLLC
implicit none

contains
  subroutine musclHLLCflux(U,flux,dt)
  use inputMod
  use HLLC
  implicit none
  
  real(dp) :: omega
  real(dp), dimension(2,3) :: delU
  real(dp), dimension(3) :: UL,UR,UbarL,UbarR,FL,FR
  real(dp), dimension(imax,3), intent(in) :: U
  real(dp), dimension(imax,3), intent(out) :: flux
  real(dp), intent(in) :: dt
  real(dp), dimension(3) :: fluxi, deli
  real(dp), dimension(3) :: WL,WR
  real(dp) :: tol
  real(dp), dimension(3) :: r
  integer :: kk
  omega=0.0_dp
  tol=1.0e-10_dp
  !call computeNumericalFlux(U(1,1),U(1,2),U(1,3),fluxi)
  !flux(1,:)=fluxi(:)
  !call computeNumericalFlux(U(imax,1),U(imax,2),U(imax,3),fluxi)
  !flux(imax,:)=fluxi(:)
  do kk=1,3
  ! Compute slopes
  ! del Ui-1/2
  delU(1,kk)=U(i,kk)-U(i-1,kk)
  !  del Ui+1/2
  delU(2,kk)=U(i+1,kk)-U(i,kk)

  do j=1,3
    if(ABS(delU(1,j)) < tol) then
      delU(1,j) = tol*SIGN(1.0_dp,delU(1,j))
      !WRITE(*,*)i,'using tol L', tol
    end if
    if(ABS(delU(2,j)) < tol) then
      delU(2,j) = tol*SIGN(1.0_dp,delU(2,j))
      !write(*,*)i,'Using tol R',tol
    end if  
  end do
 
  r(kk) = delU(1,kk)/delU(2,kk)

  ! Compute deli
  deli(kk)=0.5_dp*(1.0_dp+omega)*delU(1,kk)+0.5_dp*(1.0_dp-omega)*delU(2,kk)
  call vanLeer(r,omega,deli)
  deli(:)=0.0_dp
  !write(*,*)r(1),r(2),r(3),deli(1),deli(2),deli(3),delU(1,1),delU(2,1)
  !write(*,*)U(i-1,1),U(i,1),U(i+1,1),delU(1,1),delU(1,2),r(2)
  UL(kk) = U(i,kk) - 0.5_dp*deli(kk)
  UR(kk) = U(i,kk) + 0.5_dp*deli(kk)
  !write(*,*)UR(1),UR(2),UR(3),deli(1),r(1),r(2),r(3) 
    ! Compute numerical flux
  call computeNumericalFlux(UL(1),UL(2),UL(3),FL)
  call computeNumericalFlux(UR(1),UR(2),UR(3),FR)

  UbarL(kk) = UL(kk) + 0.5_dp*(dt/dx)*(FL(kk)-FR(kk))
  UbarR(kk) = UR(kk) + 0.5_dp*(dt/dx)*(FL(kk)-FR(kk))
  !write(*,*)i,U(i,1),UbarL(1),UbarR(1)
  ! Solve local Riemann problem using HLLC approximate method
  end do 
  ! Left state
  WL(1)=UbarL(1)
  WL(2)=UbarL(2)/UbarL(1)
  WL(3)=(gamma-1.0_dp)*(UbarL(3)-0.5_dp*UbarL(2)*WL(2))

  !  Right state
  WR(1)=UbarR(1)
  WR(2)=UbarR(2)/UbarR(1)
  WR(3)=(gamma-1.0_dp)*(UbarR(3)-0.5_dp*UbarR(2)*WR(2))

  call computeHLLCflux(WL,WR,fluxi)
  flux(i,1)=fluxi(1)
  flux(i,2)=fluxi(2)
  flux(i,3)=fluxi(3)
  !write(*,*) i,flux(i,1),flux(i,2),flux(i,3)
  end subroutine musclHLLCflux

  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U3=rho*(0.5_dp*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux

  subroutine vanLeer(r,omega,deli)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(inout) :: deli
  real(dp),dimension(3) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xivl,xiR
  do j=1,3
    if(r(j)<=0.0_dp) then
      xivl=0.0_dp
    else
      xiR=2.0_dp/(1.0_dp-omega+(1.0_dp+omega)*r(j)) 
      xivl=min((2.0_dp*r(j))/(1.0_dp+r(j)),xiR)
    end if 
    deli(j)=deli(j)*xivl
  end do
  end subroutine vanLeer
end module musclHLLC
