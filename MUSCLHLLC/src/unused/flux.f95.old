module FLUX

contains
  subroutine computeFlux()
  use inputMod
  use HLLC
  implicit none
  real(dp),dimension(imax,3),intent(in)::U
  real(dp),dimension(imax,3),intent(out)::flux
  real(dp),intent(in)::dt
  real(dp) :: omega
  real(dp),dimension(2,3) :: delU
  real(dp),dimension(3) :: UL,UR,UbarL,UbarR,FL,FR
  real(dp),dimension(imax,3), intent(in) :: U
  real(dp),dimension(imax,3), intent(out) :: flux
  real(dp),intent(in) :: dt
  real(dp),dimension(3) :: fluxi, deli
  real(dp),dimension(3) :: WL,WR
  real(dp) :: tol
  real(dp),dimension(3) :: r
  integer::j,l

  do=i=1,imax-1
    omega=0.0_dp
    tol=1.0e-10_dp
    do j=1,3
      delU(1,j)=U(i,j)-U(i-1,j)
      delU(2,j)=U(i+1,j)-U(i,j)
   
      do l=1,2
        if(ABS(delU(l,j)) < tol) then
          delU(l,j)=tol*SIGN(1.0_dp,delU(l,j))
        end if
      end do
    
      r(j)=delU(1,j)/delU(2,j)
    
      deli(j)=0.5_dp*(1.0_dp+omega)*delU(1,j)&
              +0.5_dp*(1.0_dp-omega)*delU(2,j)
      call valLeer(r,omega,deli)
      UL(j)=U(i,j)-0.5_dp*deli(j)
      UR(j)=U(i,j)+0.5_dp*deli(j)
    
      call computeNumericalFlux(UL(1),UL(2),UL(3),FL)
      call computeNumericalFlux(UR(1),UR(2),UR(3),FR)
      UbarL(kk) = UL(kk) + 0.5_dp*(dt/dx)*(FL(kk)-FR(kk))
      UbarR(kk) = UR(kk) + 0.5_dp*(dt/dx)*(FL(kk)-FR(kk))
    end do
    ! Solve local Riemann problem using HLLC approximate method
    ! Left state
    WL(1)=UbarL(1)
    WL(2)=UbarL(2)/UbarL(1)
    WL(3)=(gamma-1.0_dp)*(UbarL(3)-0.5_dp*UbarL(2)*WL(2))
    !  Right state
    WR(1)=UbarR(1)
    WR(2)=UbarR(2)/UbarR(1)
    WR(3)=(gamma-1.0_dp)*(UbarR(3)-0.5_dp*UbarR(2)*WR(2))
    call computeHLLCflux(WL,WR,fluxi)
    flux(i,1)=fluxi(1)
    flux(i,2)=fluxi(2)
    flux(i,3)=fluxi(3)
  end do
  end subroutine computeFlux()

  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U3=rho*(0.5_dp*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux

  subroutine vanLeer(r,omega,deli)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(inout) :: deli
  real(dp),dimension(3) :: r
  real(dp),intent(in) :: omega
  real(dp) :: xivl,xiR
  do j=1,3
    if(r(j)<=0.0_dp) then
      xivl=0.0_dp
    else
      xiR=2.0_dp/(1.0_dp-omega+(1.0_dp+omega)*r(j))
      xivl=min((2.0_dp*r(j))/(1.0_dp+r(j)),xiR)
    end if
    deli(j)=deli(j)*xivl
  end do
  end subroutine vanLeer
end module FLUX
