#!/bin/gnuplot -persist
 set terminal postscript eps enhanced "Times,30" solid
 set output "test1_all.eps"
 set termoption dashed
 #set nokey
 set key samplen .05 spacing 0.85 font "Times,30"
 set key opaque
 set size 2.05,2.05
 set origin 0,0
 set multiplot
 set tmargin 0.6
 set bmargin 0.6
 set rmargin 0.0
 set lmargin 0.0
 #set autoscale
 set offsets graph 0.0, graph 0.0, graph 0.095, graph 0.025

 set xrange [0:1]
 
 set key top right
 set size 0.8,0.8
 set origin 0.15,1.125
 set title "Density" offset 0.0,0.0
 p "dataFiles/godunov/test1.dat" u 1:2  every 9 title "Godunov" axes x1y1 w p lt 1 lc 1 lw 10,\
   "dataFiles/roe/test1.dat" u 1:2 every 9 title "Roe" axes x1y1 w p lt 2 lc 3 lw 1,\
   "dataFiles/hllc/test1.dat" u 1:2 every 9 title "HLLC" axes x1y1 w p lt 4 lc 7 lw 1,\
   "dataFiles/laxwendroff/test1.dat" u 1:2 every 9 title "Lax-Wendroff" axes x1y1 w p lt 8 lc 4 lw 1,\
   "dataFiles/laxfriedrichs/test1.dat" u 1:2 every 9 title "Lax-Friedrichs" axes x1y1 w p lt 6 lc 5 lw 1,\
    "exact/test1.dat" u 1:2  axes x1y1 w l lt 4 lc 7 lw 0.5 notitle

 #set key bottom left
 set nokey
 set size 0.8,0.8
 set origin 1.15,1.125
 set title "Velocity" offset 0.0,0.0
 p "dataFiles/godunov/test1.dat" u 1:3  every 9 title "Godunov" axes x1y1 w p lt 1 lc 1 lw 10,\
   "dataFiles/roe/test1.dat" u 1:3 every 9 title "Roe" axes x1y1 w p lt 2 lc 3 lw 1,\
   "dataFiles/hllc/test1.dat" u 1:3 every 9 title "HLLC" axes x1y1 w p lt 4 lc 7 lw 1,\
   "dataFiles/laxwendroff/test1.dat" u 1:3 every 9 title "Lax-Wendroff" axes x1y1 w p lt 8 lc 4 lw 1,\
   "dataFiles/laxfriedrichs/test1.dat" u 1:3 every 9 title "Lax-Friedrichs" axes x1y1 w p lt 6 lc 5 lw 1,\
    "exact/test1.dat" u 1:3  axes x1y1 w l lt 4 lc 7 lw 0.5 notitle
 #set key top left
 set size 0.8,0.8
 set origin 1.15,0.125
 set title "Internal Energy" offset 0.0,0.0
 p "dataFiles/godunov/test1.dat" u 1:5  every 9 title "Godunov" axes x1y1 w p lt 1 lc 1 lw 10,\
   "dataFiles/roe/test1.dat" u 1:5 every 9 title "Roe" axes x1y1 w p lt 2 lc 3 lw 1,\
   "dataFiles/hllc/test1.dat" u 1:5 every 9 title "HLLC" axes x1y1 w p lt 4 lc 7 lw 1,\
   "dataFiles/laxwendroff/test1.dat" u 1:5 every 9 title "Lax-Wendroff" axes x1y1 w p lt 8 lc 4 lw 1,\
   "dataFiles/laxfriedrichs/test1.dat" u 1:5 every 9 title "Lax-Friedrichs" axes x1y1 w p lt 6 lc 5 lw 1,\
    "exact/test1.dat" u 1:5  axes x1y1 w l lt 4 lc 7 lw 0.5 notitle
 #set key top right
 set size 0.8,0.8
 set origin 0.15,0.125
 set title "Pressure" offset 0.0,0.0
 p "dataFiles/godunov/test1.dat" u 1:4  every 9 title "Godunov" axes x1y1 w p lt 1 lc 1 lw 10,\
   "dataFiles/roe/test1.dat" u 1:4 every 9 title "Roe" axes x1y1 w p lt 2 lc 3 lw 1,\
   "dataFiles/hllc/test1.dat" u 1:4 every 9 title "HLLC" axes x1y1 w p lt 4 lc 7 lw 1,\
   "dataFiles/laxwendroff/test1.dat" u 1:4 every 9 title "Lax-Wendroff" axes x1y1 w p lt 8 lc 4 lw 1,\
   "dataFiles/laxfriedrichs/test1.dat" u 1:4 every 9 title "Lax-Friedrichs" axes x1y1 w p lt 6 lc 5 lw 1,\
    "exact/test1.dat" u 1:4  axes x1y1 w l lt 4 lc 7 lw 0.5 notitle
 
 unset multiplot
 reset 
