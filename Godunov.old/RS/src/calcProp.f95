SUBROUTINE calcProp(WL,WR,gamma,aL,aR,S,us,ps,state,WS)
USE HRmod
IMPLICIT NONE

! Data dictionary, cosntants
REAL,DIMENSION(3),INTENT(in)::WL,WR
REAL,INTENT(in)::S,gamma,aL,aR,us,ps

! Data dicionary, variables
REAL,DIMENSION(3),INTENT(out)::WS
REAL::shl,as,stl,PR,SL,SR,Str,Shr
INTEGER,DIMENSION(2),INTENT(out)::state

IF(S<=us) THEN
  ! Left side of contact
  IF(ps>WL(3)) THEN
  ! Left shock
  state(1)=0
  PR=ps/WL(3)
  SL=WL(2)-aL*SQRT(const(2)*PR+const(3))
    IF(SL>=S) THEN
    ! Left state
      WS(1)=WL(1)
      WS(2)=WL(2)
      WS(3)=WL(3)
    ELSE
    ! Left * State
      CALL shock(WL,const,ps,us,WS)
    END IF
  ELSE
  ! Left rarefaction
  state(1)=1
  Shl=WL(2)-aL
  CALL rarefaction(WL,gamma,aL,const,ps,WS,as)
  Stl=us-as
    IF(Shl>=S) THEN
    ! Left state
      WS(1)=WL(1)
      WS(2)=WL(2)
      WS(3)=WL(3)
    ELSEIF((Shl<=S) .AND. (S<=Stl)) THEN
    ! Inside rarefaction fan
      CALL INrarefactionL(WL,aL,const,S,WS)
    ELSE
    ! Left * State
      WS(2)=us
      WS(3)=ps
    END IF
  END IF
ELSE
  ! Right side of contact
  IF(ps>WR(3)) THEN 
  PR=ps/WR(3)
  SR=WR(2)+aR*SQRT(const(2)*PR+const(3))
  ! Right shock
  state(2)=0
    IF(SR<=S) THEN
    ! Right state
      WS(1)=WR(1)
      WS(2)=WR(2)
      WS(3)=WR(3)
    ELSE
    ! Right * State
      CALL shock(WR,const,ps,us,WS)
    END IF
  ELSE
  ! Right rarefaction
  state(2)=1
  Shr=WR(2)+aR
  CALL rarefaction(WR,gamma,aR,const,ps,WS,as)
  Str=us+as
    IF(Shr<=S) THEN 
    ! Right state
      WS(1)=WR(1)
      WS(2)=WR(2)
      WS(3)=WR(3)
    ELSEIF((Str<=S) .AND. (S<=Shr)) THEN
    ! Inside rarefaction fan
      CALL INrarefactionR(WR,aR,const,S,WS)
    ELSE
    ! Left * State
      WS(2)=us
      WS(3)=ps
    END IF
  END IF
END IF

END SUBROUTINE
