SUBROUTINE imposeCFL(U,dt)
USE inputMod
IMPLICIT NONE

REAL,DIMENSION(imax,3),INTENT(in)::U
REAL,INTENT(out)::dt
REAL::Smax

! Compute maximum predicted wave speed
CALL computeSmax(U,Smax)

! Compute time step
dt=CFL*dx/Smax

! Check for time step exceeding end time
IF((dt+t)>tout) THEN
  dt=tout-t
END IF

END SUBROUTINE imposeCFL
