SUBROUTINE updateCells(U,flux,dt)
USE inputMod
IMPLICIT NONE

REAL,DIMENSION(imax,3),INTENT(inout)::U
REAL,DIMENSION(imax,3),INTENT(in)::flux
REAL,INTENT(in)::dt

DO i=2,(imax)
  DO j=1,3
    U(i,j)=U(i,j)+(dt/dx)*(flux(i-1,j)-flux(i,j))
  END DO
END DO

END SUBROUTINE updateCells
