PROGRAM SHOCKTUBE
USE inputMod
IMPLICIT NONE

! Data dictionary
REAL,ALLOCATABLE,DIMENSION(:,:)::U,flux
REAL::dt
INTEGER::k
INTEGER::case
INTEGER::counter=0

! Read input file
CALL inputs(iWL,iWR,gamma,Len,DiscP,M,tout,CFL,dx,maxiter,imax)

ALLOCATE(U(imax,3))
ALLOCATE(flux(imax,3))

! Initialize domain
CALL initializeDomain(U)

write(*,*)'Select Case'
write(*,*)'(1) - Godunovs Method'
write(*,*)'(2) - HLLC Approximate Method'
write(*,*)'(3) - Roe Approximate Method'
write(*,*)'(4) - Lax-Friedrich'
write(*,*)'(5) - Lax-Wendroff'
write(*,*)
read(*,*)case

! March solution forward in time
DO k=1,maxiter

  ! Set boundary conditions
  CALL imposeBoundaries(U)

  ! Impose CFL condition
  CALL imposeCFL(U,dt)
  
  ! Step time forward
  t=t+dt
  
  ! Compute intercell fluxes
  CALL computeFlux(case,U,flux,dt)
  
  ! Update cells
  CALL updateCellS(U,flux,dt)
  
  IF(counter==100) THEN
    WRITE(*,*)'Iteration',k,'t=',t,'dt=',dt
    counter=0
  END IF 
  counter=counter+1
  ! Check if output time is reached
  IF(t>=tout) THEN
    ! Write solution to file
    CALL imposeBoundaries(U)
    CALL writeSolution(U)
    WRITE(*,*)'time = ',t
    WRITE(*,*)'Output time reached, exiting'
    EXIT
  END IF
END DO

CALL SYSTEM('gnuplot plotProp.plt')
END PROGRAM SHOCKTUBE

