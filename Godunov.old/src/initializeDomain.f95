!---------------------------------------------!
!
!---------------------------------------------!
SUBROUTINE initializeDomain(U)!,flux)
USE inputMod
IMPLICIT NONE

REAL,DIMENSION(imax,3),INTENT(out)::U
REAL::x=0.0
REAL::se,E

! Initialize domain
DO i=1,imax
  IF(x<DiscP) THEN
    U(i,1)=iWL(1)
    U(i,2)=iWL(2)*iWL(1)
    se=iWL(3)/((gamma-1.)*iWL(1))
    E=iWL(1)*(0.5*(iWL(2)**2.)+se)
    U(i,3)=E
  ELSE
    U(i,1)=iWR(1)
    U(i,2)=iWR(2)*iWR(1)
    se=iWR(3)/((gamma-1.)*iWR(1))
    E=iWR(1)*(0.5*(iWR(2)**2.)+se)
    U(i,3)=E
  END IF
  x=dx*(REAL(i)-1.)
END DO
END SUBROUTINE initializeDomain
