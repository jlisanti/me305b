!------------------------------------------------------!
! Exact Riemann Solver for 1D Euler Equation
!------------------------------------------------------!
MODULE ExactRiemannSolver
IMPLICIT NONE

! Data dictionary, constants

! Data dictionary, variables
REAL::ps,us
REAL::S !,x,dx
!REAL::En
REAL,DIMENSION(3)::WS
INTEGER,DIMENSION(2)::state

CONTAINS
  SUBROUTINE RiemannSolver(WL,WR,WS)
  USE initializeMod
  USE errorMod
  USE HRmod
  IMPLICIT NONE

  ! Initialize problem
  CALL init(WL,WR,gamma,aL,aR,A,B)
  CALL HRconstants(gamma,const)

  ! Check for vacuum
  IF(((2./(gamma-1.))*(aL+aR)) < (WL(2)-WR(2))) THEN
    error=1
    CALL errorF(error)
  END IF

  ! Solve for pressure and velocity in star state
  CALL calcPUS(WL,WR,A,B,gamma,aL,aR,ps,us)

  !dx=Len/REAL(M)

  ! Compute solution
  !OPEN(UNIT=2,FILE='output.dat',STATUS='replace',ACTION='write')

  S = 0.0
  CALL calcProp(WL,WR,gamma,aL,aR,S,us,ps,state,WS)
  !En=WS(3)/(WS(1)/(gamma-1.))
  !WRITE(2,*)x,WS(1),WS(2),WS(3),En,TS(1),TS(2),TS(3),TS(4)

  !IF((state(1)==0) .AND.(state(2)==0)) THEN
  !  WRITE(*,*)'Two shock solution'
  !ELSEIF((state(1)==1) .AND.(state(2)==1)) THEN
  !  WRITE(*,*)'Two rarefaction solution'
  !ELSE
  !  IF(state(1)==0) THEN
  !    WRITE(*,*)'Left shock, right rarefaction'
  !  ELSE
  !    WRITE(*,*)'Right shock, left rarefaction'
  !  END IF
  !END IF

  !CALL SYSTEM('gnuplot plotProp.plt')
  !CALL SYSTEM('open fig.eps')

  END SUBROUTINE RiemannSolver
  
  SUBROUTINE init(WL,WR,gamma,aL,aR,A,B)
    USE errorMod
    IMPLICIT NONE
    REAL,DIMENSION(3),INTENT(in)::WL,WR
    REAL,DIMENSION(2),INTENT(out)::A,B
    REAL,INTENT(out)::aL,aR
    REAL,INTENT(in)::gamma
   
    ! Check inputs
    DO i=1,3
      IF(ISNAN(WL(i))) THEN
        error=1+i
        CALL errorF(error)
      END IF
      IF(ISNAN(WR(i))) THEN
        error=4+i
        CALL errorF(error)
      END IF
      IF((WL(i)<0.0) .AND. (i/=2)) THEN
        error=7+i
        CALL errorF(error)
      END IF
      IF((WR(i)<0.0) .AND. (i/=2)) THEN
        error=8+i
        CALL errorF(error)
      END IF
    END DO

    ! Compute sound speeds for left and right states
    CALL soundSpeed(WL(1),WL(3),gamma,aL)
    CALL soundSpeed(WR(1),WR(3),gamma,aR)

    ! Compute constants for left and right states
    A(1)=constA(WL(1))
    B(1)=constB(WL(3))
    A(2)=constA(WR(1))
    B(2)=constB(WR(3))
  END SUBROUTINE inputs

  FUNCTION constA(rho)
    REAL::rho,constA
    constA=2./((gamma+1.)*rho)
  END FUNCTION constA

  FUNCTION constB(p)
    REAL::p,constB
    constB=((gamma-1.)/(gamma+1.))*p
  END FUNCTION constB

  SUBROUTINE calcPUS(WL,WR,A,B,gamma,aL,aR,p,u)
  USE errorMod
  IMPLICIT NONE

  ! Data dictionary, constants
  REAL,DIMENSION(3),INTENT(in)::WL,WR
  REAL,DIMENSION(2),INTENT(in)::A,B
  REAL,INTENT(in)::gamma,aL,aR
  REAL :: tol = 1.e-6
  INTEGER :: maxiter = 10000

  ! Data dictionary, variables
  REAL,INTENT(out) :: p,u
  REAL :: pOld
  REAL :: po
  REAL :: uDiff
  REAL :: fL,fR,dfL,dfR
  REAL :: delp
  INTEGER :: i

  ! Determine guess value
  CALL calcPguess(WL,WR,A,B,aL,aR,gamma,po)
  pOld = po

  ! Compute difference in velocity
  uDiff = WR(2)-WL(2)

  DO i=1,maxiter
    CALL calcf(gamma,aR,A(2),B(2),WR(3),WR(1),pOld,fR,dfR)
    CALL calcf(gamma,aL,A(1),B(1),WL(3),WL(1),pOld,fL,dfL)
    p = pOld - (fL+fR+uDiff)/(dfL+dfR)
    delp = 2.*ABS((p-pOld)/(p+pOld))
    IF(delp < tol) THEN
      EXIT
    ENDIF
    IF(p < 0.) p = tol
      pOld = p
    IF(i==maxiter) THEN
      error=50
      CALL errorF(error)
    END IF
  END DO
  u = 0.5*(WL(2)+WR(2))+.5*(fR-fL)
  END SUBROUTINE calcPUS

  SUBROUTINE calcPguess(WL,WR,A,B,aL,aR,gamma,po)
  IMPLICIT NONE

  ! Data dictionary, constants
  REAL,DIMENSION(3),INTENT(in)::WL,WR
  REAL,DIMENSION(2),INTENT(in)::A,B
  REAL,INTENT(in)::aL,aR,gamma

  ! Data dictionary, varibales
  REAL :: c1,c2
  REAL :: gL,gR
  REAL :: pMin,pMax,qMax
  REAL :: ppv
  REAL,INTENT(out) :: po

  ! Compute necessary quantities
  pMin = MIN(WL(3),WR(3))
  pMax = MIN(WL(3),WR(3))
  qMax = pMax/pMin

  ppv = .5*(WL(3)+WR(3))-0.25*(WR(2)-WL(2))*(WL(1)+WR(1))*(aL+aR)

  IF((qMax<2.).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
    po = ppv
  ELSEIF(ppv<pMin) THEN
    ! Two-Rarefaction 
    c1 = (gamma-1.)/(2.*gamma)
    c2 = (2.*gamma)/(gamma-1.)
    po = (((aL+aR-.5*(gamma-1.)*(WR(2)-WL(2)))/((((aL/WL(3)))**c1)&
          +(aR/WR(3))**c1)))**c2
  ELSE
    ! Two-Shock
    gL=SQRT(A(1)/(ppv+B(1)))
    gR=SQRT(A(2)/(ppv+B(2)))
    po=(gL*WL(3)+gR*WR(3)-(WR(2)-WL(2)))/(gL+gR)
  END IF

  END SUBROUTINE calcPguess 

  SUBROUTINE calcProp(WL,WR,gamma,aL,aR,S,us,ps,state,WS)
  USE HRmod
  IMPLICIT NONE

  ! Data dictionary, cosntants
  REAL,DIMENSION(3),INTENT(in)::WL,WR
  REAL,INTENT(in)::S,gamma,aL,aR,us,ps

  ! Data dicionary, variables
  REAL,DIMENSION(3),INTENT(out)::WS
  REAL::shl,as,stl,PR,SL,SR,Str,Shr
  INTEGER,DIMENSION(2),INTENT(out)::state

  IF(S<=us) THEN
    ! Left side of contact
    IF(ps>WL(3)) THEN
    ! Left shock
    state(1)=0
    PR=ps/WL(3)
    SL=WL(2)-aL*SQRT(const(2)*PR+const(3))
      IF(SL>=S) THEN
      ! Left state
        WS(1)=WL(1)
        WS(2)=WL(2)
        WS(3)=WL(3)
      ELSE
      ! Left * State
        CALL shock(WL,const,ps,us,WS)
      END IF
    ELSE
    ! Left rarefaction
    state(1)=1
    Shl=WL(2)-aL
    CALL rarefaction(WL,gamma,aL,const,ps,WS,as)
    Stl=us-as
      IF(Shl>=S) THEN
      ! Left state
        WS(1)=WL(1)
        WS(2)=WL(2)
        WS(3)=WL(3)
      ELSEIF((Shl<=S) .AND. (S<=Stl)) THEN
      ! Inside rarefaction fan
        CALL INrarefactionL(WL,aL,const,S,WS)
      ELSE
      ! Left * State
        WS(2)=us
        WS(3)=ps
      END IF
    END IF
  ELSE
   ! Right side of contact
    IF(ps>WR(3)) THEN 
    PR=ps/WR(3)
    SR=WR(2)+aR*SQRT(const(2)*PR+const(3))
    ! Right shock
    state(2)=0
      IF(SR<=S) THEN
      ! Right state
        WS(1)=WR(1)
        WS(2)=WR(2)
        WS(3)=WR(3)
      ELSE
      ! Right * State
        CALL shock(WR,const,ps,us,WS)
      END IF
    ELSE
    ! Right rarefaction
    state(2)=1
    Shr=WR(2)+aR
    CALL rarefaction(WR,gamma,aR,const,ps,WS,as)
    Str=us+as
      IF(Shr<=S) THEN
      ! Right state
        WS(1)=WR(1)
        WS(2)=WR(2)
        WS(3)=WR(3)
      ELSEIF((Str<=S) .AND. (S<=Shr)) THEN
      ! Inside rarefaction fan
        CALL INrarefactionR(WR,aR,const,S,WS)
      ELSE
      ! Left * State
        WS(2)=us
        WS(3)=ps
      END IF
    END IF
  END IF

  END SUBROUTINE

END MODULE ExactRiemannSolver
