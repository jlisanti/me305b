MODULE HRmod
IMPLICIT NONE

! Data dictionary, variables
REAL,DIMENSION(7)::const
! Data dictionary, constants

CONTAINS
  SUBROUTINE HRconstants(gamma,const)
  IMPLICIT NONE
  REAL,INTENT(in)::gamma
  REAL,DIMENSION(7),INTENT(out)::const
  
  const(1)=(gamma-1.)/(gamma+1.)
  const(2)=(gamma+1.)/(2.*gamma)
  const(3)=(gamma-1.)/(2.*gamma)
  const(4)=2./(gamma+1.)
  const(5)=(gamma-1.)/(gamma+1.)
  const(6)=2./(gamma-1.)
  const(7)=(2.*gamma)/(gamma-1.)
    
  END SUBROUTINE HRconstants
  
  SUBROUTINE shock(Wk,const,ps,us,WS)
  IMPLICIT NONE
  REAL,DIMENSION(3),INTENT(in)::Wk
  REAL,DIMENSION(7),INTENT(in)::const
  REAL,INTENT(in)::ps,us
  REAL,DIMENSION(3),INTENT(inout)::WS
    WS(1)=Wk(1)*((const(1)+(ps/Wk(3)))/(const(1)*(ps/Wk(3))+1.))
    WS(2)=us
    WS(3)=ps
  END SUBROUTINE shock
  
  SUBROUTINE rarefaction(Wk,gamma,a,const,ps,WS,as)
  IMPLICIT NONE
  REAL,DIMENSION(3),INTENT(in)::Wk
  REAL,DIMENSION(7),INTENT(in)::const
  REAL,INTENT(in)::ps,gamma,a
  REAL,DIMENSION(3),INTENT(out)::WS
  REAL,INTENT(out)::as
    WS(1)=Wk(1)*(ps/Wk(3))**(1./gamma)
    as=a*((ps/Wk(3))**(1./const(7)))
  END SUBROUTINE rarefaction

  SUBROUTINE INrarefactionL(Wk,a,const,ps,us,S,WS)
  IMPLICIT NONE
  REAL,DIMENSION(3),INTENT(in)::Wk
  REAL,DIMENSION(7),INTENT(in)::const
  REAL,INTENT(in)::ps,us,a,S
  REAL,DIMENSION(3),INTENT(inout)::WS
  REAL::D
    D=const(4)+(const(5)/a)*(Wk(2)-S)
    WS(1)=Wk(1)*(D**const(6))
    WS(2)=const(4)*(a+(Wk(2)/const(6))+S)
    WS(3)=Wk(3)*(D**(const(7)))
  END SUBROUTINE INrarefactionL
  
  SUBROUTINE INrarefactionR(Wk,a,const,ps,us,S,WS)
  IMPLICIT NONE
  REAL,DIMENSION(3),INTENT(in)::Wk
  REAL,DIMENSION(7),INTENT(in)::const
  REAL,INTENT(in)::ps,us,a,S
  REAL,DIMENSION(3),INTENT(inout)::WS
  REAL::D
    D=const(4)-(const(5)/a)*(Wk(2)-S)
    WS(1)=Wk(1)*(D**const(6))
    WS(2)=const(4)*(-a+(Wk(2)/const(6))+S)
    WS(3)=Wk(3)*(D**(const(7)))
  END SUBROUTINE INrarefactionR
  
END MODULE HRmod
