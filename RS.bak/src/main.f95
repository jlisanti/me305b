!------------------------------------------------------!
!
!------------------------------------------------------!

PROGRAM rs
USE initializeMod
USE errorMod
USE HRmod
IMPLICIT NONE

! Data dictionary, constants

! Data dictionary, variables
REAL::ps,us
REAL::p,u,S,x,dx
REAL::En
REAL,DIMENSION(3)::WS
INTEGER,DIMENSION(2)::state

! Initialize problem
CALL inputs(WL,WR,gamma,aL,aR,A,B,Len,DiscP,M,t)
CALL HRconstants(gamma,const)

! Check for vacuum
IF(((2./(gamma-1.))*(aL+aR)) < (WL(2)-WR(2))) THEN
        error=1
        CALL errorF(error)
END IF

! Solve for pressure and velocity in star state
CALL calcPUS(WL,WR,A,B,gamma,aL,aR,ps,us)

dx=Len/REAL(M)

! Compute solution
OPEN(UNIT=2,FILE='output.dat',STATUS='replace',ACTION='write')
DO i=1,M
        x=(REAL(i)-.5)*dx
        S = (x-DiscP)/t
        CALL calcProp(WL,WR,gamma,aL,aR,S,x,t,us,ps,state,WS)
        En=WS(3)/(WS(1)/(gamma-1.))
        WRITE(2,*)x,WS(1),WS(2),WS(3),En
END DO

!IF((state(1)==0) .AND.(state(2)==0)) THEN
!  WRITE(*,*)'Two shock solution'
!ELSEIF((state(1)==1) .AND.(state(2)==1)) THEN
!  WRITE(*,*)'Two rarefaction solution'
!ELSE
!  IF(state(1)==0) THEN
!    WRITE(*,*)'Left shock, right rarefaction'
!  ELSE
!    WRITE(*,*)'Right shock, left rarefaction'
!  END IF
!END IF

CALL SYSTEM('gnuplot plotProp.plt')
CALL SYSTEM('open fig.eps')

END PROGRAM rs
