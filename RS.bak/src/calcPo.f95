SUBROUTINE calcPguess(WL,WR,A,B,aL,aR,gamma,po)
IMPLICIT NONE

! Data dictionary, constants
REAL,DIMENSION(3),INTENT(in)::WL,WR
REAL,DIMENSION(2),INTENT(in)::A,B
REAL,INTENT(in)::aL,aR,gamma

! Data dictionary, varibales
REAL :: c1,c2
REAL :: gL,gR
REAL :: pMin,pMax,qMax
REAL :: ppv
REAL,INTENT(out) :: po

! Compute necessary quantities
pMin = MIN(WL(3),WR(3))
pMax = MIN(WL(3),WR(3))
qMax = pMax/pMin

ppv = .5*(WL(3)+WR(3))-0.25*(WR(2)-WL(2))*(WL(1)+WR(1))*(aL+aR)

IF((qMax<2.).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
        po = ppv
ELSEIF(ppv<pMin) THEN
        ! Two-Rarefaction 
        c1 = (gamma-1.)/(2.*gamma)
        c2 = (2.*gamma)/(gamma-1.)
        po = (((aL+aR-.5*(gamma-1.)*(WR(2)-WL(2)))/((((aL/WL(3)))**c1)&
             +(aR/WR(3))**c1)))**c2
ELSE
        ! Two-Shock
        gL=SQRT(A(1)/(ppv+B(1)))
        gR=SQRT(A(2)/(ppv+B(2)))
        po=(gL*WL(3)+gR*WR(3)-(WR(2)-WL(2)))/(gL+gR)
END IF

END SUBROUTINE calcPguess
