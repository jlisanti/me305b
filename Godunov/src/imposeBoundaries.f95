!---------------------------------------------!
!  Impose transmissive boundaries for the 
!   1D shock tube problem
!---------------------------------------------!
SUBROUTINE imposeBoundaries(U)
USE inputMod
IMPLICIT NONE
REAL,DIMENSION(imax,3),INTENT(inout)::U

DO j=1,3
  U(1,j)=U(2,j)
  U(imax,j)=U(imax-1,j)
END DO

END SUBROUTINE imposeBoundaries
