MODULE errorMod
IMPLICIT NONE
INTEGER::error=0
CONTAINS
  SUBROUTINE errorF(error)
  IMPLICIT NONE
  INTEGER,INTENT(in)::error
  IF(error==1) THEN
    WRITE(*,*)
    WRITE(*,*)'***WARNING***'
    WRITE(*,*)'Vacuum generate by initial states'
    WRITE(*,*)'Program terminating...'
    WRITE(*,*)
    STOP
  ELSEIF((error>1) .AND. (error<50)) THEN
    WRITE(*,*)
    WRITE(*,*)'***WARNING***'
    WRITE(*,*)'Error with initial states'
    IF(error==2) THEN
      WRITE(*,*)'Left density state value is NaN'
    END IF
    IF(error==3) THEN
      WRITE(*,*)'Left velocity state value is NaN'
    END IF
    IF(error==4) THEN
      WRITE(*,*)'Left pressure state value is NaN'
    END IF
    IF(error==5) THEN
      WRITE(*,*)'Right density state value is NaN'
    END IF
    IF(error==6) THEN
      WRITE(*,*)'Right velocity state value is NaN'
    END IF
    IF(error==7) THEN
      WRITE(*,*)'Right velocity state value is NaN'
    END IF
    IF(error==8) THEN
      WRITE(*,*)'Left density state value is negative'
    END IF
    IF(error==9) THEN
      WRITE(*,*)'Right density state value is negative'
    END IF
    IF(error==10) THEN
      WRITE(*,*)'Left pressure state value is negative'
    END IF
    IF(error==11) THEN
      WRITE(*,*)'Right pressure state value is negative'
    END IF
    WRITE(*,*)
  ELSEIF(error==50) THEN
    WRITE(*,*)
    WRITE(*,*)'***WARNING***'
    WRITE(*,*)'N-R Solution to P* diverged, exiting...'
    WRITE(*,*)
    STOP
  elseif((error>=100) .and. (error<=125)) then
    write(*,*)
    write(*,*)'***WARNING***'
    write(*,*)'Problem with HLLC Solver'
    write(*,*)
    if(error==101) then
      write(*,*)'Guess P* is NAN'
      write(*,*)'Exiting...'
      write(*,*)
      stop
     endif
  END IF
  

END SUBROUTINE errorF

END MODULE errorMod
