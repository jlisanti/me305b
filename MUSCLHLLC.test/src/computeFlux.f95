subroutine computeFlux(U,flux)
use inputMod
use HLLC
implicit none

real(dp),dimension(imax,3),intent(in)::U
real(dp),dimension(imax,3),intent(out)::flux
!real(dp),intent(in)::dt
real(dp), dimension(3) :: WL,WR
real(dp), dimension(3) :: fluxi
do i=1,imax-1

  WL(1)=U(i,1)
      WL(2)=U(i,2)/U(i,1)
      WL(3)=(gamma-1.0_dp)*(U(i,3)-0.5_dp*U(i,2)*WL(2))
      !  Right state
      WR(1)=U(i+1,1)
      WR(2)=U(i+1,2)/U(i+1,1)
      WR(3)=(gamma-1.0_dp)*(U(i+1,3)-0.5_dp*U(i+1,2)*WR(2))
      call computeHLLCflux(WL,WR,fluxi)
      flux(i,1)=fluxi(1)
      flux(i,2)=fluxi(2)
      flux(i,3)=fluxi(3)

end do
!stop
end subroutine computeFlux
