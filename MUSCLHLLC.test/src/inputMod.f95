MODULE inputMod
IMPLICIT NONE

! Data dictionary
integer, parameter :: dp = selected_real_kind(15, 307)
REAL(dp),DIMENSION(3)::iWL,iWR
REAL(dp)::gamma
REAL(dp)::Len
REAL(dp)::DiscP
REAL(dp)::tout
REAL(dp)::t=0.0_dp
REAL(dp)::dx
REAL(dp)::CFL
INTEGER::M
INTEGER::i,imax
INTEGER::j
INTEGER::maxiter
CHARACTER(100)::file_name

CONTAINS
  SUBROUTINE inputs(iWL,iWR,gamma,Len,DiscP,M,tout,CFL,dx,maxiter,imax)
    USE errorMod
    IMPLICIT NONE
    REAL(dp),DIMENSION(3),INTENT(out)::iWL,iWR
    REAL(dp),INTENT(out)::gamma,Len,Discp
    REAL(dp),INTENT(out)::tout,CFL,dx
    INTEGER,INTENT(out)::M,maxiter,imax

    ! Read input data
    file_name='input.in'
    OPEN(UNIT=1,FILE=TRIM(file_name),STATUS='old',ACTION='read')
    REWIND(1)
    DO i=1,36
      IF(i==3) THEN
        READ(1,*) iWL(1)
      ELSEIF(i==5) THEN
        READ(1,*) iWL(2)
      ELSEIF(i==7) THEN
        READ(1,*) iWL(3)
      ELSEIF(i==11) THEN
        READ(1,*) iWR(1)
      ELSEIF(i==13) THEN
        READ(1,*) iWR(2)
      ELSEIF(i==15) THEN
        READ(1,*) iWR(3)
      ELSEIF(i==18) THEN
        READ(1,*) gamma
      ELSEIF(i==21) THEN
        READ(1,*) Len
      ELSEIF(i==24) THEN
        READ(1,*) DiscP
      ELSEIF(i==27) THEN
        READ(1,*) M
      ELSEIF(i==30) THEN
        READ(1,*) tout
      ELSEIF(i==33) THEN
        READ(1,*) CFL
      ELSEIF(i==36) THEN
        READ(1,*) maxiter
      ELSE
        READ(1,*)
      END IF
    END DO
    CLOSE(1)
    
    ! Check inputs
    DO i=1,3
      IF(ISNAN(iWL(i))) THEN
        error=1+i
        CALL errorF(error)
      END IF
      IF(ISNAN(iWR(i))) THEN
        error=4+i
        CALL errorF(error)
      END IF
      IF((iWL(i)<0.0) .AND. (i/=2)) THEN
        error=7+i
        CALL errorF(error)
      END IF
      IF((iWR(i)<0.0) .AND. (i/=2)) THEN
        error=8+i 
        CALL errorF(error)
      END IF
    END DO 
    
    dx=Len/REAL(M)
    imax=M
  END SUBROUTINE inputs

END MODULE inputMod
