!------------------------------------------------------------------!
!  The purpose of this subroutine is to compute numerical
!    flux using the HLLC approximate solution to the
!    Riemann problem.
!------------------------------------------------------------------!
module HLLC
use inputMod
implicit none
real(dp)::rhoL,velL,pL
real(dp)::rhoR,velR,pR
real(dp)::aL,aR

contains
  subroutine computeHLLCflux(WL,WR,flux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(in)::WL,WR
  real(dp),dimension(3),intent(out)::flux
  real(dp),dimension(3)::numericalFlux
  real(dp),dimension(3)::Ustar,Ul,Ur
  real(dp)::SL,Sstar,SR

  rhoL=WL(1)
  velL=WL(2)
  pL=WL(3)

  rhoR=WR(1)
  velR=WR(2)
  pR=WR(3)

  call soundSpeed(rhoL,pL,gamma,aL)
  call soundSpeed(rhoR,pR,gamma,aR)
  call estimateWaveSpeeds(SL,Sstar,SR)
  
  if(SL>0.0_dp) then
    call computeNumericalFlux(rhoL,velL,pL,numericalFlux)
    do j=1,3
      flux(j)=numericalFlux(j)
    end do
  end if
  
  if((SL<0.0_dp) .and. (SR>0.0_dp)) then
    if(Sstar>0.0_dp) then
      call computeNumericalFlux(rhoL,velL,pL,numericalFlux)
      call computeU(rhoL,velL,pL,Ul)
      call computeUstar(Sstar,SL,rhoL,velL,pL,Ustar)
      do j=1,3
        flux(j)=numericalFlux(j)+SL*(Ustar(j)-Ul(j))
      end do
    else
      call computeNumericalFlux(rhoR,velR,pR,numericalFlux)
      call computeU(rhoR,velR,pR,Ur)
      call computeUstar(Sstar,SR,rhoR,velR,pR,Ustar)
      do j=1,3
        flux(j)=numericalFlux(j)+SR*(Ustar(j)-Ur(j))
      end do
    end if
  end if
  
 if(SR<0.0_dp) then
   call computeNumericalFlux(rhoR,velR,pR,numericalFlux)
    do j=1,3
      flux(j)=numericalFlux(j)
    end do
  end if
  end subroutine computeHLLCflux
  
  subroutine estimateWaveSpeeds(SL,Sstar,SR)
  use inputMod
  implicit none

  ! Data dictionary
  real(dp),intent(out)::SL,Sstar,SR
  REAL(dp) :: c1,c2,c3,c4,c5,c6,c7,c8
  REAL(dp) :: gL,gR
  REAL(dp) :: pMin,pMax,qMax
  REAL(dp) :: ppv
  real(dp) ::cup
  real(dp) ::pq,PTL,PTR
  real(dp) :: po
  real(dp) ::um
  c1 = (gamma - 1.0_dp)/(2.0_dp*gamma)
  c2 = (gamma + 1.0_dp)/(2.0_dp*gamma)
  c3 = 2.0_dp*gamma/(gamma - 1.0)
  c4 = 2.0_dp/(gamma - 1.0_dp)
  c5 = 2.0_dp/(gamma + 1.0_dp)
  c6 = (gamma - 1.0_dp)/(gamma + 1.0_dp)
  c7 = (gamma - 1.0_dp)/2.0_dp
  c8 = gamma - 1.0_dp

  ! Compute necessary quantities
  pMin = MIN(pL,pR)
  pMax = MIN(pL,pR)
  qMax = pMax/pMin
  cup=0.25_dp*(rhoL+rhoR)+(aL+aR)
  ppv=0.5_dp*(pL+pR)-0.5_dp*(velL-velR)*cup
  
  IF((qMax<2.0_dp).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
    po = ppv
    um=0.5_dp*(velL+velR)+0.5_dp*(pL-pR)/cup
  ELSEIF(ppv<pMin) THEN
    ! Two-Rarefaction 
    pq  = (pL/pR)**c1
    um  = (PQ*velL/aL + velR/aR + c4*(PQ - 1.0_dp))/(PQ/aL + 1.0_dp/aR)
    PTL = 1.0_dp + c7*(velL - um)/velL
    PTR = 1.0_dp + c7*(um - velR)/aR
    po  = 0.5_dp*(pL*PTL**c3 + pR*PTR**c3)
  ELSE
    ! Two-Shock
    gL=SQRT(c5/(ppv+c6*pL))
    gR=SQRT(c5/(ppv+c6*pR))
    po=(gL*pL+gR*pR-(velR-velL))/(gL+gR)
    um=0.5_dp*(velL+velR)+0.5_dp*(gR*(po-pR)-gL*(po-pL))
  END IF

  if(po<pL) then
    SL = velL-aL
  else
    SL = velL-aL*SQRT(1.0_dp+c2*(po/pL-1.0_dp))
  endif
  Sstar=um
  if(po<pR) then
    SR = velR+aR
  else
    SR = velR+aR*SQRT(1.0_dp+c2*(po/pR-1.0_dp))
  endif
  end subroutine estimateWaveSpeeds
 
  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.)*rho)
  U3=rho*(0.5_dp*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux
  
  subroutine computeUStar(Sstar,Sk,rho,vel,p,Ustar)
  use inputMod
  implicit none
  real(dp),intent(in)::Sstar,Sk,rho,vel,p
  real(dp),dimension(3),intent(out)::Ustar
  real(dp)::A,B,C
  real(dp)::se
  se=p/((gamma-1.0_dp)*rho)
  A=rho*((Sk-vel)/(Sk-Sstar))
  B=(0.5_dp*((vel*vel))+se)
  C=Sstar+(p/(rho*(Sk-vel)))
  Ustar(1)=A
  Ustar(2)=A*Sstar
  Ustar(3)=A*(B+(Sstar-vel)*C)
  end subroutine computeUStar

  subroutine computeU(rho,vel,p,U)
  use inputMod
  implicit none
  real(dp),intent(in)::rho,vel,p
  real(dp),dimension(3),intent(out)::U
  real(dp)::specificEnergy
  U(1)=rho
  U(2)=rho*vel
  specificEnergy=p/((gamma-1.0_dp)*rho)
  U(3)=rho*(0.5_dp*vel*vel+specificEnergy)
  end subroutine computeU
end module HLLC
