module musclHLLC
implicit none

contains
  subroutine musclHLLCflux(U,dt,flux)
  use inputMod
  use HLLC
  implicit none
  
  real(dp) :: omega
  real(dp), dimension(2,3) :: delU
  real(dp), dimension(3) :: UL,UR,UbarL,UbarR,FL,FR
  real(dp), dimension(imax,3), intent(in) :: U
  real(dp), dimension(imax,3), intent(out) :: flux
  real(dp), intent(in) :: dt
  real(dp), dimension(3) :: fluxi, deli
  real(dp), dimension(3) :: WL,WR
  real(dp) :: tol
  omega=0.0_dp
  tol=1.0e-7_dp
  ! Compute slopes
  
  ! del Ui-1/2
  delU(1,:)=U(i,:)-U(i-1,:)
  ! del Ui+1/2
  delU(2,:)=U(i+1,:)-U(i,:)

  do j=1,3
    if(ABS(delU(1,j)) < tol) then
      delU(1,j) = tol*SIGN(1.0_dp,delU(1,j))
      !WRITE(*,*)i,'using tol L', tol
    end if
    if(ABS(delU(2,j)) < tol) then
      delU(2,j) = tol*SIGN(1.0_dp,delU(2,j))
      !write(*,*)i,'Using tol R',tol
    end if  
  end do
 

  ! Compute deli
  deli(:)=0.5_dp*(1.0_dp+omega)*delU(1,:)+0.5_dp*(1.0_dp-omega)*delU(2,:)
  !deli(:) = 0.0_dp 
  UL(:) = U(i,:) !- 0.5_dp*deli(:)
  UR(:) = U(i,:) !+ 0.5_dp*deli(:)
  !write(*,*)UR(1),UR(2),UR(3),deli(1) 
    ! Compute numerical flux
  call computeNumericalFlux(UL(1),UL(2),UL(3),FL)
  call computeNumericalFlux(UR(1),UR(2),UR(3),FR)

  UbarL(:) = UL(:) !+ 0.5_dp*(dt/dx)*(FL(:)-FR(:))
  UbarR(:) = UR(:) !+ 0.5_dp*(dt/dx)*(FL(:)-FR(:))
  !write(*,*)i,U(i,1),UbarL(1),UbarR(1)
  ! Solve local Riemann problem using HLLC approximate method
  WL(1)=UbarL(1)
  WL(2)=UbarL(2)/UbarL(1)
  WL(3)=(gamma-1.0_dp)*(UbarL(3)-0.5_dp*UbarL(2)*WL(2))
  !  Right state
  WR(1)=UbarR(1)
  WR(2)=UbarR(2)/UbarR(1)
  WR(3)=(gamma-1.0_dp)*(UbarR(3)-0.5_dp*UbarR(2)*WR(2))
  !write(*,*) WL(1),WL(2),WL(3),WR(1),WR(2),WR(3)
  call computeHLLCflux(WL,WR,fluxi)
  !write(*,*) fluxi(1),fluxi(2),fluxi(3)
  !write(*,*)WL(1),WL(2),WL(3)
  !write(*,*)WR(1),WR(2),WR(3)
  flux(i,1)=fluxi(1)
  flux(i,2)=fluxi(2)
  flux(i,3)=fluxi(3)
  end subroutine musclHLLCflux

  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real(dp),dimension(3),intent(out)::numericalFlux
  real(dp),intent(in)::rho,vel,p
  real(dp)::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.)*rho)
  U3=rho*(0.5*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho * vel
  numericalFlux(2) = (rho*vel)* vel + p
  numericalFlux(3) = H * vel
  end subroutine computeNumericalFlux
end module musclHLLC
