SUBROUTINE soundSpeed(rho,p,gamma,a)
IMPLICIT NONE
integer, parameter :: dp = selected_real_kind(15, 307)
REAL(dp),INTENT(in)::rho,p,gamma
REAL(dp),INTENT(out)::a
a=SQRT(gamma*p/rho)
END SUBROUTINE soundSpeed
