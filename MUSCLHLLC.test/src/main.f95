program SHOCKTUBE
use inputMod
implicit none

! Data dictionary
real(dp), allocatable, dimension(:,:)::U,flux
real(dp)::dt
integer::k
integer::counter=0

! Read input file
CALL inputs(iWL,iWR,gamma,Len,DiscP,M,tout,CFL,dx,maxiter,imax)

allocate(U(imax,3))
allocate(flux(imax,3))

! Initialize domain
call initializeDomain(U)

! March solution forward in time
DO k=1,maxiter

  ! Set boundary conditions
  CALL imposeBoundaries(U)
 
  ! Impose CFL condition
  CALL imposeCFL(U,dt)
  
  ! Step time forward
  t=t+dt
  
  ! Compute intercell fluxes
  call computeFlux(U,flux)

  ! Update cells
  CALL updateCellS(U,flux,dt)
  !stop
  IF(counter==100) THEN
    WRITE(*,*)'Iteration',k,'t=',t,'dt=',dt
    counter=0
  END IF 
  counter=counter+1
  ! Check if output time is reached
 IF(t>=tout) THEN
    ! Write solution to file
    CALL imposeBoundaries(U)
    CALL writeSolution(U)
    WRITE(*,*)'time = ',t
    WRITE(*,*)'Output time reached, exiting'
    EXIT
  END IF
 !if(k==10) then
 !CALL writeSolution(U)
 !CALL SYSTEM('gnuplot plotProp.plt')
 !stop
 !end if
END DO

CALL SYSTEM('gnuplot plotProp.plt')
END PROGRAM SHOCKTUBE

