!------------------------------------------------------------------!
!  This module provides an approximate solution to a local 
!    Riemann problem using the ROE method with Harten–Hyman 
!    Entropy Fix
!------------------------------------------------------------------!
module ROE

contains
  subroutine computeRoeFlux(Uil,Uir,dt,flux)
  use inputMod
  implicit none
  real,dimension(3),intent(in)::Uil,Uir
  real,dimension(3),intent(out)::flux
  real::tol
  real::rhoL,uL,pL,HL,rhoR,uR,pR,HR
  real::rm,rho,um,Hm,am,du,dp
  real::val,sn,si,cflm,umm,amm,ak
  real,dimension(3)::lamb,numericalFlux
  real::const
  real::sml,smr
  real::aL,aR
  real,intent(in)::dt
  tol=0.1
  const = gamma - 1.0
  
  ! Set initial states for local Riemann problem

  ! Left state
  rhoL=Uil(1)
  uL=Uil(2)/Uil(1)
  pL=(gamma-1.)*(Uil(3)-0.5*Uil(2)*uL)
  call soundSpeed(rhoL,pL,gamma,aL)
  HL=(UiL(3)+pL)/rhoL
  ! Right state
  rhoR=Uir(1)
  uR=Uir(2)/Uir(1)
  pR=(gamma-1.)*(Uir(3)-0.5*Uir(2)*uR)
  call soundSpeed(rhoR,pR,gamma,aR)
  HR=(UiR(3)+pR)/rhoR

  ! Compute Roe Averages
  rm=SQRT(rhoR/rhoL)
  rho=rm*rhoL
  um=(uL+rm*uR)/(1.+Rm)
  Hm=(HL+rm*HR)/(1.+Rm)
  am=SQRT(const*(Hm-0.5*um*um))

  ! Compute differences 
  du=uR-uL
  dp=pR-pL
   
  if(um>0.0) then
    val=um-am
    sn=val
    ak=(dp-rho*am*du)/(2.*am*am)
    cflm=val*dt/dx
    if(ABS(cflm)<tol) then
      ! Apply entropy fix
      si=1.0
      call computeStarValues(si,rhoL,uL,Uil(3),ak,um,am,Hm,umm,amm)
      sml=uL-aL
      smr=umm-amm
      if((sml<0.0) .and. (smr>0.0)) then
        sn=sml*(smr-val)/(smr-sml)
      end if
    end if
    if(sn<0.0) then
      ! Compute left eigenvectors
      lamb(1)=1.0
      lamb(2)=um-am
      lamb(3)=Hm-um*am
      call computeNumericalFlux(rhoL,uL,pL,numericalFlux)
      do j=1,3
       flux(j)=numericalFlux(j)+sn*ak*lamb(j)
      end do
    else
      call computeNumericalFlux(rhoL,uL,pL,numericalFlux)
      do j=1,3
        flux(j)=numericalFlux(j)
      end do
    end if
  else
    val=um+am
    sn=val
    ak=(dp+rho*am*du)/(2.*am*am)
    cflm=val*dt/dx
    if(ABS(cflm)<tol) then
      ! Apply entropy fix
      si=-1.0
      call computeStarValues(si,rhoR,uR,Uir(3),ak,um,am,Hm,umm,amm)
      sml=umm+amm
      smr=uR+aR
      if((sml<0.0) .and. (smr>0.0)) then
        sn=smr*(val-sml)/(smr-sml)
      end if
    end if
    if(sn>0.0) then
      ! Compute right eigenvectors
      lamb(1)=1.0
      lamb(2)=um+am
      lamb(3)=Hm+um*am
      call computeNumericalFlux(rhoR,uR,pR,numericalFlux)
      do j=1,3
       flux(j)=numericalFlux(j)-sn*ak*lamb(j)
      end do
    else
      call computeNumericalFlux(rhoR,uR,pR,numericalFlux)
      do j=1,3
        flux(j)=numericalFlux(j)
      end do
    end if
  end if
  end subroutine computeRoeFlux
  
  subroutine computeStarValues(si,rho,u,E,ak,um,am,Hm,umm,amm)
  use inputMod
  implicit none
  real,intent(in)::si,rho,u,E,ak,um,am,Hm
  real,intent(out)::umm,amm
  real::rhomk,pm
  real::const
  const = gamma - 1.0
  rhomk = rho + si*ak
  umm = (rho*u  + si*ak*(um - si*aM))/rhomk
  pm  = const*(E + si*ak*(Hm - si*um*aM) - 0.5*rhomk*umm*umm)
  amm = SQRT(gamma*pm/rhomk)
  
  end subroutine computeStarValues

  subroutine computeNumericalFlux(rho,vel,p,numericalFlux)
  use inputMod
  implicit none
  real,dimension(3),intent(out)::numericalFlux
  real,intent(in)::rho,vel,p
  real::H,specificEnergy,U3
  specificEnergy=p/((gamma-1.)*rho)
  U3=rho*(0.5*vel*vel+specificEnergy)
  H=U3+p
  numericalFlux(1) = rho*vel
  numericalFlux(2) = (rho*vel)*vel+p
  numericalFlux(3) = H*vel
  end subroutine computeNumericalFlux
end module ROE
