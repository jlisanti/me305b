subroutine computeFlux(case,U,flux,dt)
use inputMod
use ExactRiemannSolver
use HLLC
use ROE
implicit none

real,dimension(imax,3),intent(in)::U
real,dimension(imax,3),intent(out)::flux
real,dimension(imax,3)::fluxf
real,intent(in)::dt
real,dimension(3)::fluxi,Uil,Uir
real,dimension(imax,3)::Ui
integer,intent(in)::case
real::se
real::u1,u2,u3
real::density,velocity,pressure,enthalpy

if((case>=1) .and. (case<=3)) then
  do i=1,(imax-1)
    if(case==1) then
      ! Define left and right states for local Riemann Problem
      ! Left state
      WL(1)=U(i,1)
      WL(2)=U(i,2)/U(i,1)
      WL(3)=(gamma-1.)*(U(i,3)-0.5*U(i,2)*WL(2))
      !  Right state
      WR(1)=U(i+1,1)
      WR(2)=U(i+1,2)/U(i+1,1)
      WR(3)=(gamma-1.)*(U(i+1,3)-0.5*U(i+1,2)*WR(2))

      ! Solve local Riemann Problem
      call RiemannSolver(WL,WR,gamma,WS)
      u1=WS(1)
      u2=WS(1)*WS(2)
      se=WS(3)/((gamma-1.)*WS(1))
      u3=WS(1)*(0.5*((WS(2)**2.))+se)
 
      flux(i,1)=u2
      flux(i,2)=0.5*(3.-gamma)*((u2**2.)/u1)+(gamma-1.)*u3
      flux(i,3)=gamma*(u2/u1)*u3-.5*(gamma-1.)*((u2**3.)/(u1**2.))
 
    elseif(case==2) then
      ! Define left and right states for local Riemann Problem
      ! Left state
      WL(1)=U(i,1)
      WL(2)=U(i,2)/U(i,1)
      WL(3)=(gamma-1.)*(U(i,3)-0.5*U(i,2)*WL(2))
      !  Right state
      WR(1)=U(i+1,1)
      WR(2)=U(i+1,2)/U(i+1,1)
      WR(3)=(gamma-1.)*(U(i+1,3)-0.5*U(i+1,2)*WR(2))
      call computeHLLCflux(WL,WR,fluxi)
      flux(i,1)=fluxi(1)
      flux(i,2)=fluxi(2)
      flux(i,3)=fluxi(3)
    
    elseif(case==3) then
      ! Define left and right states for local Riemann Problem
      do j=1,3
        Uil(j)=U(i,j)
        Uir(j)=U(i+1,j)
      end do
      call computeRoeFlux(Uil,Uir,dt,fluxi)
      flux(i,1)=fluxi(1)
      flux(i,2)=fluxi(2)
      flux(i,3)=fluxi(3)
      !write(*,*)flux(i,1),flux(i,2),flux(i,3)
    end if
  end do
end if

if(case==4) then
  ! Lax-Friedrich
  do i=1,(imax)
    density = U(i,1)
    velocity = U(i,2)/U(i,1)
    pressure = (gamma-1.0)*(U(i,3)-0.5*density*velocity*velocity)
    enthalpy = U(i,3)+pressure
 
    fluxf(i,1) = density*velocity
    fluxf(i,2) = (density*velocity)*velocity+pressure
    fluxf(i,3) = enthalpy*velocity
  end do
  do i=1,imax
   do j=1,3
      flux(i,j) = 0.5*(fluxf(i,j)+fluxf(i+1,j))+0.5*(dx/dt)*(U(i,j)-U(i+1,j))
   end do 
  end do
end if

if(case==5) then
  ! Lax-Wendroff
  do i=1,imax
    density = U(i,1)
    velocity = U(i,2)/U(i,1)
    pressure = (gamma-1.0)*(U(i,3)-0.5*density*velocity*velocity)
    enthalpy = U(i,3)+pressure

    fluxf(i,1) = density*velocity
    fluxf(i,2) = (density*velocity)*velocity+pressure
    fluxf(i,3) = enthalpy*velocity
    do j=1,3
      Ui(i,j)=.5*(U(i,j)+U(i+1,j))+0.5*(dt/dx)*(fluxf(i,j)-fluxf(i+1,j))
    end do
  end do
  do i=1,imax
    density = Ui(i,1)
    velocity = Ui(i,2)/Ui(i,1)
    pressure = (gamma-1.0)*(Ui(i,3)-0.5*density*velocity*velocity)
    enthalpy = Ui(i,3)+pressure
    
    flux(i,1) = density*velocity
    flux(i,2) = (density*velocity)*velocity+pressure
    flux(i,3) = enthalpy*velocity
  end do
end if
end subroutine computeFlux
