MODULE initialize
IMPLICIT NONE

! Data dictionary, variables
REAL :: pL              ! Initial pressure, left state
REAL :: pR              ! Initial pressure, right state
REAL :: rhoL            ! Initial density, left state
REAL :: rhoR            ! Initial density, right state
REAL :: uL              ! Initial velocity, left state
REAL :: uR              ! Initial velocity, right state
REAL :: gamma           ! Ratio of specific heats
REAL :: cL,cR           ! Initial sound speeds, right & left states

REAL :: Len             ! Domain length
REAL :: DiscP           ! Initial position of discontinuity
INTEGER :: M            ! Number of cells
INTEGER :: i
CHARACTER(100) :: file_name

! Data dictionary, constants
REAL :: aL,aR,bL,bR 


WRITE(*,*)'Enter input filename'
READ(*,*)file_name

! Read input data
OPEN(UNIT=1,FILE=TRIM(file_name),STATUS='old',ACTION='read')
REWIND(1)
DO i=1,27
        IF(i==3) THEN
                READ(1,*) pL
        ELSEIF(i==5) THEN
                READ(1,*) rhoL
        ELSEIF(i==7) THEN
                READ(1,*) uL
        ELSEIF(i==11) THEN
                READ(1,*) pR
        ELSEIF(i==13) THEN
                READ(1,*) rhoR
        ELSEIF(i==15) THEN
                READ(1,*) uR
        ELSEIF(i==18) THEN
                READ(1,*) gamma
        ELSEIF(i==21) THEN
                READ(1,*) Len
        ELSEIF(i==24) THEN
                READ(1,*) DiscP
        ELSEIF(i==27) THEN
                READ(1,*) M
        ELSE
                READ(1,*)
        END IF
END DO
CLOSE(1)

! Compute necessary constants
aL = 2./((gamma+1.)*rhoL)
aR = 2./((gamma+1.)*rhoR)
bL = ((gamma-1.)/(gamma+1.))*pL
bR = ((gamma-1.)/(gamma+1.))*pR

! Compute sound speeds
cL = SQRT(gamma*pL/rhoL)
cR = SQRT(gamma*pR/rhoR)

END MODULE initialize
