PROGRAM rs
USE initialize
IMPLICIT NONE

! Data dictionary, constants
REAL :: Len		! Domain length
REAL :: DiscP		! Initial position of discontinuity
REAL :: x,dx
REAL :: t
INTEGER :: M		! Number of cells

! Data dictionaray, variables
REAL :: u,p,S
REAL :: rhos,us,ps

CALL initialize(const,Len,DiscP,M)
WRITE(*,*)M
! Determine pressure sign
IF(((2./(gamma-1.))*(cL+cR))) < (uR-uL)) THEN
	WRITE(*,*)'Vacuum generate by initial states'
	WRITE(*,*)'Program terminating...'
	STOP
END IF

! Solve for pressure and velocity in star region
CALL calcPU(p,u)

!WRITE(*,*)'Pstar:',p,'Ustar:',u

WRITE(*,*)'Enter time at which solution should be computed'
READ(*,*) t

dx=Len/REAL(M)

! Compute solution
OPEN(UNIT=2,FILE='output.dat',STATUS='replace',ACTION='write')
DO i=1,M
	x=(REAL(i)-.5)*dx
	S = (x-DiscP)/t
        CALL calcProp(p,u,S,x,t,rhos,us,ps)
	WRITE(2,*)x,rhos,us,ps
END DO

CALL SYSTEM('gnuplot plotProp.plt')
CALL SYSTEM('open fig.eps')
END PROGRAM rs
