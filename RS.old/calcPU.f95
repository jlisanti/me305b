SUBROUTINE calcPU(p,u)
USE initialize
IMPLICIT NONE

! Data dictionary, constants

! Data dictionary, variables
REAL,INTENT(inout) :: p,u
REAL :: pOld
REAL :: pIni
REAL :: uDiff
REAL :: fL,fR,dfL,dfR
REAL :: delp
REAL :: tol = 1.e-6
INTEGER :: i
INTEGER :: maxiter = 100000000

! Determine guess value
CALL calcpIni(pIni)
pOld = pIni

! Compute difference in velocity
uDiff = uR-uL

DO i=1,maxiter
	CALL calcf(gamma,cR,aR,bR,pR,rhoR,pOld,fR,dfR)
	CALL calcf(gamma,cL,aL,bL,pL,rhoL,pOld,fL,dfL)
	p = pOld - (fL+fR+uDiff)/(dfL+dfR)
	delp = 2.*ABS((p-pOld)/(p+pOld))
	IF(delp < tol) THEN 
		WRITE(*,*)'Converged after',i,'iterations'
		EXIT
	ENDIF
	IF(p < 0.) p = tol
	pOld = p
	IF(i==maxiter) THEN
		WRITE(*,*)'Solution to P divereged, exiting...'
	END IF
END DO

u = 0.5*(uL+uR+.5*(fR-fL)
END SUBROUTINE calcPU
