#!/bin/gnuplot -persist 
set terminal postscript eps enhanced color solid 20
 set output "test_nogood.eps"
 
 set nokey
 set size 1,1
 set origin 0,0
 
 set multiplot
 
 set tmargin 0.5
 set bmargin 0.5
 set rmargin 1.0
 set lmargin 5.0
 
 set xrange [-25:25]
 set ylabel "f(x)"
 set noxtics
 set noytics
 
 set size 1,0.20
 set origin 0.0,0.76
 set title "sin(x)" offset 0,-1.0
 plot sin(x) w l lt 1 lw 2 
 set size 1,0.20 
 set origin 0.0,0.54
 set title "cos(x)" offset 0,-1.0
 plot cos(x) w l lt 9 lw 2 
 set size 1,0.20 
 set origin 0.0,0.32
 set title "sin(x)/x" offset 0,-1.0
 plot sin(x)/x w l lt 3 lw 2 
 set size 1,0.20 
 set origin 0.0,0.10
 set xlabel "x"
 set title "x*sin(x)" offset 0,-1.0
 plot x*sin(x) w l lt 8 lw 2 
 
 unset multiplot
 
 reset
