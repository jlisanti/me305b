SUBROUTINE calcf(gamma,ck,Ak,Bk,pk,rhok,p,fk,dfk)
IMPLICIT NONE

! Data dictionary, constants
REAL, INTENT(in) :: gamma,ck,aK,bK,pK,rhok,p
REAL :: A,B,C
! Data dictionary, variables
REAL, INTENT(out) :: fk,dfk

IF(p < pk) THEN
	! Rarefaction wave
	A = (2.*ck)/(gamma-1.)
	B = (gamma-1.)/(2.*gamma)
	fk = A*((p/pk)**B-1.)
	C = -1.*(gamma+1.)/(2.*gamma)
	dfk = (1./(rhok*ck))*((p/pk)**C)
ELSE
	! Shock wave
	fk = (p-pk)*SQRT(ak/(p+bk))
	dfk = (1.-((p-pk)/(2.*(bk+p))))*SQRT(ak/(bk+p))
END IF

END SUBROUTINE calcf
