SUBROUTINE calcpIni(pIni)
USE initialize
IMPLICIT NONE

! Data dictionary, constants

! Data dictionary, varibales
REAL :: A,B
REAL :: gL,gR
REAL :: pMin,pMax,qMax
REAL :: ppv
REAL,INTENT(out) :: pIni

! Compute necessary quantities
pMin = MIN(pL,pR)
pMax = MIN(pL,pR)
qMax = pMax/pMin

ppv = .5*(pL+pR)-0.25*(uR-uL)*(rhoL+rhoR)&
		*(const(12)+const(13))

IF((qMax<2.).AND.(pMin<ppv).AND.(ppv<pMax)) THEN
	WRITE(*,*)'ppv'
	pIni = ppv
ELSEIF(ppv<pMin) THEN
	! Two-Rarefaction 
	WRITE(*,*)'Two-Rarefaction'
	A = (gamma-1.)/(2.*gamma)
	B = (2.*gamma)/(gamma-1.)
	pIni = ((cL+cR-.5*(gamma-1.)&
	*(uR-uL))/((cL/pL))**A)&
	+(cR/pR)**A)))**B
ELSE
	! Two-Shock
	WRITE(*,*)'Two-shock'
	gL=SQRT(aL/(ppv+bL))
	gR=SQRT(aR/(ppv+bR))
	pIni=(gL*pL+gR*pR-(uR-uL))/(gL+gR)
END IF

END SUBROUTINE calcpIni
