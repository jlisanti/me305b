SUBROUTINE calcProp(const,p,u,S,x,t,rhos,us,ps)
IMPLICIT NONE

! Data dictionary, variables
REAL,INTENT(in) :: S
REAL,INTENT(inout) :: p,u,x,t
REAL,INTENT(out) :: rhos,ps,us
REAL :: c1,c2,c3,c4,c5,c6,c7,c8
REAL :: PR,SL,SR,csr,csl,shl,shr,stl,str
REAL :: cup,pv

! Data dictionary, constants
REAL,DIMENSION(13),INTENT(in) :: const

c1=(const(7)-1.)/(2.*const(7))
c2=1./const(7)
c3=2./(const(7)+1.)
c4=(const(7)-1.)/(const(7)+1.)
c5=(const(7)-1.)/2.
c6=2./(const(7)-1.)
c7=(2.*const(7))/(const(7)-1.)
c8=(const(7)+1.)/(2*const(7))

cup=0.25*(const(2)+const(5))*(const(12)+const(13))
pv=0.5*(const(1)+const(4))+0.5*(const(3)-const(6))*cup
IF(S<u) THEN
	IF(pv<const(1)) THEN
		! Left rarefaction
		shl = const(3)-const(12)
		IF(S<Shl) THEN
			! Properties in left state
			rhos=const(2)
			us=const(3)
			ps=const(1)
		ELSE
			csl=const(12)*(pv/const(1))**c1
			stl=u-csl
			IF(s>stl) THEN
				! Properties in left * state
				rhos=const(2)*(pv/const(1))**c2
				us=u
				ps=p
			ELSE
				! Properties in left fan
				rhos=const(2)*(c3+(c4/const(12))*(const(3)-(x/t)))**c6
				us=c3*(const(12)+c5*const(3)+(x/t))
				ps=const(1)*(c3+(c4/const(12))*(const(3)-(x/t)))**c7
			END IF
		END IF
	ELSE
		! Left shock
		PR=pv/const(1)
		SL=const(3)-const(12)*SQRT(c8*PR+c1)
		IF(S<SL) THEN
			! Properties in left state
			rhos=const(2)
                        us=const(3)
                        ps=const(1)
		ELSE
			! Properties in left * state
			rhos=const(2)*((PR+c4)/(c4*PR+1.))
			us=const(3)
                        ps=const(1)
		END IF
	END IF
ELSE
	IF(p>const(4)) THEN
		! Right shock
                PR=pv/const(4)
                SR=const(6)+const(13)*SQRT(c8*PR+c1)
                IF(S>SR) THEN
                        ! Properties in right state
                        rhos=const(5)
                        us=const(6)
                        ps=const(4)
                ELSE
                        ! Properties in left * state
                        rhos=const(2)*((PR+c4)/(c4*PR+1.))
                        us=const(6)
                        ps=const(4)
                END IF
	ELSE
		! Right rarefaction
                shr = const(6)+const(13)
                IF(S<Shr) THEN
                        ! Properties in left state
                        rhos=const(5)
                        us=const(6)
                        ps=const(4)
                ELSE
                        csR=const(13)*(pv/const(4))**c1
                        str=u-csr
                        IF(s>str) THEN
                                ! Properties in left * state
                                rhos=const(5)*(pv/const(4))**c2
                                us=u
                                ps=p
                        ELSE
                                ! Properties in left fan
                                rhos=const(5)*(c3+(c4/const(13))*(const(6)-(x/t)))**c6
                                us=c3*(const(13)+c5*const(6)+(x/t))
                                ps=const(4)*(c3+(c4/const(13))*(const(6)-(x/t)))**c7
                        END IF
                END IF
	END IF
END IF

END SUBROUTINE calcProp
