#!/bin/gnuplot -persist
 #set terminal epslatex
 set terminal postscript eps enhanced "Times,20" solid
 set output "entropy.eps"
 set termoption dashed
 #set nokey
 set key opaque
 #set size 2.05,2.05
 #set origin 0,0
 set y2range [5.4:7.5]
 set y2tics ("6.5" 6.5, "7.5" 7.5)
 set y2tics nomirror
 set autoscale  y
 #set autoscale y2
 #set multiplot
 #set tmargin 0.6
 #set bmargin 0.6
 #set rmargin 0.0
 #set lmargin 0.0
 #set autoscale
 set ytics (".5" 0.5, "1" 1.0)
 set offsets graph 0.0, graph 0.0, graph 0.095, graph 0.025
  set ylabel "Density"
 set y2label "u + 2a/{/Symbol g}-1"
 set xrange [0:1]
 
 set key top center
 #set size 0.8,0.8
 #set origin 0.15,1.125
 p "entropyCheck.dat" u 1:2  notitle axes x1y1 w l lt 1 lc 1 lw 3,\
   "entropyCheck.dat" u 1:3  notitle axes x1y2 w l lt 2 lc 7 lw 3 

 #set key top right
 #set size 0.8,0.8
 #set origin 1.15,1.125
 #set title "Velocity" offset 0.0,0.0
 #p "output.dat" u 1:3 axes x1y1 w l lt 1 lc 1 lw 3 notitle,\
 #  "output.dat" u 1:7 axes x1y1 w l lt 4 lc 7 lw 0.5 title "Toro u_*"

 #set size 0.8,0.8
 #set origin 1.15,0.125
 #set title "Internal Energy" offset 0.0,0.0
 #p "output.dat" u 1:5 axes x1y1 w l lt 1 lc 1 lw 3 notitle

 #set size 0.8,0.8
 #set origin 0.15,0.125
 #set title "Pressure" offset 0.0,0.0
 #p "output.dat" u 1:4 axes x1y1 w l lt 1 lc 1 lw 3 notitle,\
 #  "output.dat" u 1:6 axes x1y1 w l lt 4 lc 7 lw 0.5 title "Toro p_*"
 
 #unset multiplot
 reset 
