PROGRAM checkEntropy
IMPLICIT NONE

REAL::x,a,gamma,u,p,rho
INTEGER::status
gamma=1.4
OPEN(UNIT=1,FILE='test1.dat',STATUS='old',IOSTAT=status)
REWIND(1)
OPEN(UNIT=2,FILE='entropyCheck.dat',STATUS='replace')
REWIND(2)
DO
  READ(1,*,IOSTAT=status) x,rho,u,p
  IF(status /= 0) EXIT
  a=SQRT(gamma*P/rho)
  WRITE(2,*)x,rho,(u+(2.*a)/(gamma-1.))
END DO
CLOSE(2)
CLOSE(1)
END PROGRAM checkEntropy
