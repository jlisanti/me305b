MODULE initializeMod
IMPLICIT NONE

! Data dictionary, variables
REAL,DIMENSION(3)::WL,WR
REAL,DIMENSION(2)::A,B              ! Constants, 1 = Left & 2 = right
REAL,DIMENSION(4)::TS
REAL::aL,aR
REAL::gamma
REAL::Len
REAL::DiscP
REAL::t
INTEGER::M
INTEGER::i
CHARACTER(100)::file_name

! Data dictionary, constanst

CONTAINS
  SUBROUTINE inputs(WL,WR,gamma,aL,aR,A,B,Len,DiscP,M,t,TS)
    USE errorMod
    IMPLICIT NONE
    REAL,DIMENSION(3),INTENT(out)::WL,WR
    REAL,DIMENSION(2),INTENT(out)::A,B
    REAL,DIMENSION(4),INTENT(out)::TS
    REAL,INTENT(out)::gamma,aL,aR,Len,Discp
    REAL,INTENT(out)::t
    INTEGER,INTENT(out)::M
    ! Prompt user for input file
    !WRITE(*,*)'Enter input filen name'
    !READ(*,*)file_name
    ! Read input data
    file_name='input.in'
    OPEN(UNIT=1,FILE=TRIM(file_name),STATUS='old',ACTION='read')
    !OPEN(UNIT=1,FILE='input.dat',STATUS='old',ACTION='read')
    REWIND(1)
    DO i=1,33
      IF(i==3) THEN
        READ(1,*) WL(1)
      ELSEIF(i==5) THEN
        READ(1,*) WL(2)
      ELSEIF(i==7) THEN
        READ(1,*) WL(3)
      ELSEIF(i==11) THEN
        READ(1,*) WR(1)
      ELSEIF(i==13) THEN
        READ(1,*) WR(2)
      ELSEIF(i==15) THEN
        READ(1,*) WR(3)
      ELSEIF(i==18) THEN
        READ(1,*) gamma
      ELSEIF(i==21) THEN
        READ(1,*) Len
      ELSEIF(i==24) THEN
        READ(1,*) DiscP
      ELSEIF(i==27) THEN
        READ(1,*) M
      ELSEIF(i==30) THEN
        READ(1,*) t
      ELSEIF(i==33) THEN
        READ(1,*) TS(1),TS(2),TS(3),TS(4)
      ELSE
        READ(1,*)
      END IF
    END DO
    CLOSE(1)
    
    ! Check inputs
    DO i=1,3
      IF(ISNAN(WL(i))) THEN
        error=1+i
        CALL errorF(error)
      END IF
      IF(ISNAN(WR(i))) THEN
        error=4+i
        CALL errorF(error)
      END IF
      IF((WL(i)<0.0) .AND. (i/=2)) THEN
        error=7+i
        CALL errorF(error)
      END IF
      IF((WR(i)<0.0) .AND. (i/=2)) THEN
        error=8+i 
        CALL errorF(error)
      END IF
    END DO 
    
    ! Compute sound speeds for left and right states
    CALL soundSpeed(WL(1),WL(3),gamma,aL)
    CALL soundSpeed(WR(1),WR(3),gamma,aR)
    
    ! Compute constants for left and right states
    A(1)=constA(WL(1))
    B(1)=constB(WL(3))
    A(2)=constA(WR(1))
    B(2)=constB(WR(3))
  END SUBROUTINE inputs
  
  FUNCTION constA(rho)
    REAL::rho,constA
    constA=2./((gamma+1.)*rho)
  END FUNCTION constA

  FUNCTION constB(p)
    REAL::p,constB
    constB=((gamma-1.)/(gamma+1.))*p
  END FUNCTION constB

END MODULE initializeMod
