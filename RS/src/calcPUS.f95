SUBROUTINE calcPUS(WL,WR,A,B,gamma,aL,aR,p,u)
USE errorMod
IMPLICIT NONE

! Data dictionary, constants
REAL,DIMENSION(3),INTENT(in)::WL,WR
REAL,DIMENSION(2),INTENT(in)::A,B
REAL,INTENT(in)::gamma,aL,aR
REAL :: tol = 1.e-6
INTEGER :: maxiter = 10000

! Data dictionary, variables
REAL,INTENT(out) :: p,u
REAL :: pOld
REAL :: po
REAL :: uDiff
REAL :: fL,fR,dfL,dfR
REAL :: delp
INTEGER :: i

! Determine guess value
CALL calcPguess(WL,WR,A,B,aL,aR,gamma,po)
pOld = po

! Compute difference in velocity
uDiff = WR(2)-WL(2)

DO i=1,maxiter
  CALL calcf(gamma,aR,A(2),B(2),WR(3),WR(1),pOld,fR,dfR)
  CALL calcf(gamma,aL,A(1),B(1),WL(3),WL(1),pOld,fL,dfL)
  p = pOld - (fL+fR+uDiff)/(dfL+dfR)
  delp = 2.*ABS((p-pOld)/(p+pOld))
  IF(delp < tol) THEN
    EXIT
  ENDIF
  IF(p < 0.) p = tol
    pOld = p
    IF(i==maxiter) THEN
      error=50
      CALL errorF(error)
    END IF
END DO
u = 0.5*(WL(2)+WR(2))+.5*(fR-fL)
END SUBROUTINE calcPUS
